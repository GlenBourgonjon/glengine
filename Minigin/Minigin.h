#pragma once

struct SDL_Window;

namespace Glengine
{
	class Minigin final
	{
	public:
		void Initialize( const char* windowTitle, const int width, const int height );
		void Run( );

	private:
		void Cleanup( );

		SDL_Window* m_Window{};
	};
}