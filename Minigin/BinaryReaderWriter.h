#pragma once

#include <vector>
#include <fstream>
#include <string>

namespace Glengine
{
	template <typename T>
	class BinaryReaderWriter final
	{
	public:
		BinaryReaderWriter( ) = delete;

		static bool Read( std::istream& file, T& data );
		static bool Read( std::istream& file, std::vector<T>& data );
		static bool Write( std::ostream& file, const T& data );
		static bool Write( std::ostream& file, const std::vector<T>& data );
	};

	template <typename T>
	bool BinaryReaderWriter<T>::Read( std::istream& file, T& data )
	{
		if (!std::is_pod<T>::value)
		{
			return false;
		}

		file.read((char*)&data, sizeof(data));

		return true;
	}

	template <>
	inline bool BinaryReaderWriter<std::string>::Read( std::istream& file, std::string& data )
	{
		uint32_t size;
		file.read((char*)&size, sizeof(size));
		char* pBuffer = new char[size];
		file.read(pBuffer, size);
		data.append(pBuffer, size);
		delete[] pBuffer;

		return true;
	}

	template <typename T>
	bool BinaryReaderWriter<T>::Read( std::istream& file, std::vector<T>& data )
	{
		uint32_t size;
		file.read((char*)&size, sizeof(size));
		data.clear();
		data.reserve(size);
		for (size_t i{ 0 }; i < size; ++i)
		{
			T element;
			if (!Read(file, element))
			{
				return false;
			}
			data.push_back(element);
		}

		return true;
	}

	template <typename T>
	bool BinaryReaderWriter<T>::Write( std::ostream& file, const T& data )
	{
		if (!std::is_pod <T>::value)
		{
			return false;
		}

		file.write((const char*)&data, sizeof(data));

		return true;
	}

	template <>
	inline bool BinaryReaderWriter<std::string>::Write( std::ostream& file, const std::string& data )
	{
		const uint32_t size = uint32_t(data.size());

		file.write((const char*)&size, sizeof(size));
		file.write(data.c_str(), size);

		return true;
	}

	template <typename T>
	bool BinaryReaderWriter<T>::Write( std::ostream& file, const std::vector<T>& data )
	{
		const uint32_t size = uint32_t(data.size());

		file.write((const char*)&size, sizeof(size));
		for (const T& dataElement : data)
		{
			if (!Write(file, dataElement))
			{
				return false;
			}
		}

		return true;
	}
}