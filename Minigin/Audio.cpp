#include "MiniginPCH.h"
#include "Audio.h"
#include "SoundStream.h"
#include "SoundEffect.h"

namespace Glengine
{
	NormalAudioSystem::NormalAudioSystem( )
		: AudioSystem{ }
		, m_pEffects{ }
		, m_pStreams{ }
	{}

	NormalAudioSystem::~NormalAudioSystem( )
	{
		for (auto effect : m_pEffects)
		{
			delete effect.second;
		}

		for (auto stream : m_pStreams)
		{
			delete stream.second;
		}
	}

	void NormalAudioSystem::PlayEffect( const std::string& soundPath )
	{
		if (m_pEffects.count(soundPath) == 0)
			m_pEffects.insert(std::make_pair(soundPath, new SoundEffect{ soundPath }));

		m_pEffects.at(soundPath)->Play(0);
	}

	void NormalAudioSystem::StopEffects( )
	{
		Mix_HaltChannel(-1);
	}

	int NormalAudioSystem::GetEffectVolume( const std::string& soundPath ) const
	{
		if (m_pEffects.count(soundPath) == 0)
			return 0;
		else
			return m_pEffects.at(soundPath)->GetVolume();
	}

	void NormalAudioSystem::SetEffectVolume( const std::string& soundPath, const int volume )
	{
		if (m_pEffects.count(soundPath) > 0)
			m_pEffects.at(soundPath)->SetVolume(volume);
	}

	void NormalAudioSystem::PlayStream( const std::string& soundPath, const bool loop )
	{
		if (m_pStreams.count(soundPath) == 0)
			m_pStreams.insert(std::make_pair(soundPath, new SoundStream{ soundPath }));

		m_pStreams.at(soundPath)->Play(loop);
	}

	void NormalAudioSystem::PauseStream( )
	{
		Mix_PauseMusic();
	}
	
	void NormalAudioSystem::ResumeStream( )
	{
		Mix_ResumeMusic();
	}

	void NormalAudioSystem::StopStream( )
	{
		Mix_HaltMusic();
	}

	bool NormalAudioSystem::IsStreaming( ) const
	{
		return (Mix_PlayingMusic() != 0);
	}

	int NormalAudioSystem::GetStreamVolume( ) const
	{
		return Mix_VolumeMusic(-1);
	}

	void NormalAudioSystem::SetStreamVolume( const int volume )
	{
		Mix_VolumeMusic(volume);
	}
}