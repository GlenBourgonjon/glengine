#include "MiniginPCH.h"
#include "BehaviorTreeManager.h"
#include "BehaviorTree.h"

#include <thread>
#include <future>

namespace Glengine
{
	BehaviorTreeComponent* BehaviorTreeManager::AddComponent( GameObject* pObject )
	{
		if (m_Components.count(pObject) == 0)
		{
			m_Components.emplace(std::make_pair(pObject, BehaviorTreeComponent{ pObject }));
			BehaviorTreeComponent* pComponent = &m_Components.at(pObject);
			return pComponent;
		}
		else
			return nullptr;
	}

	BehaviorTreeComponent* BehaviorTreeManager::GetComponent( GameObject* pObject )
	{
		if (m_Components.count(pObject) > 0)
			return &m_Components.at(pObject);
		else
			return nullptr;
	}

	bool BehaviorTreeManager::RemoveComponent( GameObject* pObject )
	{
		return (m_Components.erase(pObject) > 0);
	}

	void BehaviorTreeManager::RemoveAllComponents( )
	{
		m_Components.clear();
	}

	void BehaviorTreeManager::Update( )
	{
		auto lambda = [](std::pair<GameObject* const, BehaviorTreeComponent>* pair)
		{
			return pair->second.m_pTree->Update(pair->first);
		};
		std::vector<std::future<BehaviorState>> futures(m_Components.size());
		int i = 0;
		for (std::pair<GameObject* const, BehaviorTreeComponent>& pair : m_Components)
		{
			futures[i] = std::async(std::launch::async, lambda, &pair);
			++i;
		}
		for (std::future<BehaviorState>& future : futures)
		{
			future.get();
		}
	}
}