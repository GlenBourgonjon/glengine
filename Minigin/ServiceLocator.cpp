#include "MiniginPCH.h"
#include "ServiceLocator.h"

namespace Glengine
{
	AudioSystem* ServiceLocator::m_pAudioSystem{ nullptr };
	NullAudioSystem ServiceLocator::m_DefaultAudioSystem{ };
}