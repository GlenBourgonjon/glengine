#pragma once

#include <vector>
#include <memory>

#include "Singleton.h"
#include "Component.h"

#pragma warning(push)
#pragma warning (disable:4201)
#include <glm/vec2.hpp>
#pragma warning(pop)

namespace Glengine
{
	class Texture2D;

	class TextureComponent final : public Component
	{
	public:
		void SetFlipped( const bool value )
		{
			m_Flipped = value;
		}

		void SetAnimated( const bool value )
		{
			m_Animated = value;
		}

		void SetTexture( const int textureID )
		{
			m_TextureID = textureID;
		}

		void SetSprite( const int spriteID )
		{
			m_SpriteID = spriteID;

			if (m_MinSpriteID == -1)
				return;
			else if (m_SpriteID < m_MinSpriteID)
				m_SpriteID = m_MinSpriteID;
			else if (m_SpriteID > m_MaxSpriteID)
				m_SpriteID = m_MaxSpriteID;
		}

		void SetMinMaxSprite( const int min, const int max )
		{
			m_MinSpriteID = min;
			m_MaxSpriteID = max;

			if (m_SpriteID < m_MinSpriteID)
				SetSprite(m_MinSpriteID);
			else if (m_SpriteID > m_MaxSpriteID)
				SetSprite(m_MaxSpriteID);
		}

		void SetSpriteInterval( const int interval )
		{
			m_SpriteInterval = interval;
		}

		void SetAnimationSpeed( const float speed )
		{
			m_AnimationSpeed = speed;
		}

		void SetPivot( const glm::vec2& pivot )
		{
			m_Pivot = pivot;
		}

	private:
		explicit TextureComponent( GameObject* pObject )
			: Component{ pObject }
			, m_Flipped{ false }
			, m_Animated{ false }
			, m_TextureID{ -1 }
			, m_SpriteID{ 0 }
			, m_MinSpriteID{ -1 }
			, m_MaxSpriteID{ -1 }
			, m_SpriteInterval{ 1 }
			, m_AnimationSpeed{ 1.f }
			, m_CurrentAnimationTime{ 0.f }
			, m_Pivot{ 0.f, 0.f }
		{}

		bool m_Flipped;
		bool m_Animated;

		int m_TextureID;
		int m_SpriteID;

		int m_MinSpriteID;
		int m_MaxSpriteID;
		int m_SpriteInterval;
		float m_AnimationSpeed;
		float m_CurrentAnimationTime;

		glm::vec2 m_Pivot;

		friend class TextureManager;
	};

	class TextureManager final : public Singleton<TextureManager>
	{
	public:
		TextureComponent* AddComponent( GameObject* pObject );
		TextureComponent* GetComponent( GameObject* pObject );
		bool RemoveComponent( GameObject* pObject );
		void RemoveAllComponents( );

		int AddTexture( const std::string& fileName, const int columns = 1, const int rows = 1 );

	private:
		TextureManager( ) = default;

		void Update( );
		void Render( );

		std::vector<TextureComponent> m_Components;
		std::vector<std::shared_ptr<Texture2D>> m_pTextures;

		friend class Singleton<TextureManager>;
		friend class Scene;
	};
}