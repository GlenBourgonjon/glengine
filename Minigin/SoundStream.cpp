#include "MiniginPCH.h"
#include "SoundStream.h"

namespace Glengine
{
	SoundStream::SoundStream( const std::string& path )
		: m_pMixMusic{ Mix_LoadMUS(path.c_str()) }
	{
		if (m_pMixMusic == nullptr)
		{
			std::string errorMsg = "SoundStream: Failed to load " + path + ",\nSDL_mixer Error: " + Mix_GetError( );
			std::cerr << errorMsg;
		}
	}

	SoundStream::~SoundStream( )
	{
		Mix_FreeMusic(m_pMixMusic);
		m_pMixMusic = nullptr;
	}

	bool SoundStream::IsLoaded( ) const
	{
		return m_pMixMusic != nullptr;
	}

	bool SoundStream::Play( bool repeat ) const
	{
		if (m_pMixMusic != nullptr)
		{
			int result{ Mix_PlayMusic(m_pMixMusic, repeat ? -1 : 1) };
			return (result == 0);
		}
		else
			return false;
	}
}