#include "MiniginPCH.h"
#include "ScoreManager.h"
#include "GameObject.h"

namespace Glengine
{
	ScoreComponent* ScoreManager::AddComponent( GameObject* pObject )
	{
		if (m_Components.count(pObject) == 0)
		{
			m_Components.emplace(std::make_pair(pObject, ScoreComponent{ pObject }));
			ScoreComponent* pComponent = &m_Components.at(pObject);
			return pComponent;
		}
		else
			return nullptr;
	}

	ScoreComponent* ScoreManager::GetComponent( GameObject* pObject )
	{
		if (m_Components.count(pObject) > 0)
			return &m_Components.at(pObject);
		else
			return nullptr;
	}

	bool ScoreManager::RemoveComponent( GameObject* pObject )
	{
		return (m_Components.erase(pObject) > 0);
	}

	void ScoreManager::RemoveAllComponents( )
	{
		m_Components.clear();
	}

	void ScoreManager::Update( )
	{
		for (std::pair<GameObject* const, ScoreComponent>& pair : m_Components)
		{
			ScoreComponent& component = pair.second;
			if (component.m_NewScore != component.m_Score)
			{
				component.NotifyObservers(component.m_NewScore - component.m_Score);
				component.m_Score = component.m_NewScore;
			}
		}
	}
}