#include "MiniginPCH.h"
#include "LogicManager.h"
#include "GameObject.h"

namespace Glengine
{
	LogicComponent* LogicManager::AddEarlyComponent( GameObject* pObject )
	{
		if (m_EarlyComponents.count(pObject) == 0)
		{
			m_EarlyComponents.emplace(std::make_pair(pObject, LogicComponent{ pObject }));
			LogicComponent* pComponent = &m_EarlyComponents.at(pObject);
			return pComponent;
		}
		else
			return nullptr;
	}

	LogicComponent* LogicManager::GetEarlyComponent( GameObject* pObject )
	{
		if (m_EarlyComponents.count(pObject) > 0)
			return &m_EarlyComponents.at(pObject);
		else
			return nullptr;
	}

	bool LogicManager::RemoveEarlyComponent( GameObject* pObject )
	{
		return (m_EarlyComponents.erase(pObject) > 0);
	}

	void LogicManager::EarlyUpdate( )
	{
		for (std::pair<GameObject* const, LogicComponent>& pair : m_EarlyComponents)
		{
			if (pair.second.m_Callback != nullptr)
				pair.second.m_Callback(pair.first);
		}
	}

	LogicComponent* LogicManager::AddLateComponent( GameObject* pObject )
	{
		if (m_LateComponents.count(pObject) == 0)
		{
			m_LateComponents.emplace(std::make_pair(pObject, LogicComponent{ pObject }));
			LogicComponent* pComponent = &m_LateComponents.at(pObject);
			return pComponent;
		}
		else
			return nullptr;
	}

	LogicComponent* LogicManager::GetLateComponent( GameObject* pObject )
	{
		if (m_LateComponents.count(pObject) > 0)
			return &m_LateComponents.at(pObject);
		else
			return nullptr;
	}

	bool LogicManager::RemoveLateComponent( GameObject* pObject )
	{
		return (m_LateComponents.erase(pObject) > 0);
	}

	void LogicManager::LateUpdate( )
	{
		for (std::pair<GameObject* const, LogicComponent>& pair : m_LateComponents)
		{
			if (pair.second.m_Callback != nullptr)
				pair.second.m_Callback(pair.first);
		}
	}

	void LogicManager::RemoveAllComponents( )
	{
		m_EarlyComponents.clear();
		m_LateComponents.clear();
	}
}