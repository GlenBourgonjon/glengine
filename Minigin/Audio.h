#pragma once

#include <unordered_map>

namespace Glengine
{
	class SoundStream;
	class SoundEffect;

	class AudioSystem
	{
	public:
		// Constructor / Destructor
		AudioSystem( ) = default;
		virtual ~AudioSystem( ) = default;

		// Sound functionality
		virtual void PlayEffect( const std::string& soundPath ) = 0;
		virtual void StopEffects( ) = 0;
		virtual int GetEffectVolume( const std::string& soundPath ) const = 0;
		virtual void SetEffectVolume( const std::string& soundPath, const int volume ) = 0;
		virtual void PlayStream( const std::string& soundPath, const bool loop ) = 0;
		virtual void PauseStream( ) = 0;
		virtual void ResumeStream( ) = 0;
		virtual void StopStream( ) = 0;
		virtual bool IsStreaming( ) const = 0;
		virtual int GetStreamVolume( ) const = 0;
		virtual void SetStreamVolume( const int volume ) = 0;
	};

	class NullAudioSystem final : public AudioSystem
	{
	public:
		// Constructor / Destructor
		NullAudioSystem( )
			: AudioSystem{ }
		{}
		~NullAudioSystem( ) = default;

		// Sound functionality
		void PlayEffect( const std::string& ) override
		{}
		void StopEffects( ) override
		{}
		int GetEffectVolume( const std::string& ) const override
		{
			return 0;
		}
		void SetEffectVolume( const std::string&, const int ) override
		{}
		void PlayStream( const std::string&, const bool ) override
		{}
		void PauseStream( ) override
		{}
		void ResumeStream( ) override
		{}
		void StopStream( ) override
		{}
		bool IsStreaming( ) const override
		{
			return false;
		}
		int GetStreamVolume( ) const override
		{
			return 0;
		}
		void SetStreamVolume( const int ) override
		{}
	};

	class NormalAudioSystem final : public AudioSystem
	{
	public:
		// Constructor / Destructor
		NormalAudioSystem( );
		~NormalAudioSystem( );

		// Sound functionality
		void PlayEffect( const std::string& soundPath ) override;
		void StopEffects( ) override;
		int GetEffectVolume( const std::string& soundPath ) const override;
		void SetEffectVolume(const std::string& soundPath, const int volume ) override;
		void PlayStream( const std::string& soundPath, const bool loop ) override;
		void PauseStream( ) override;
		void ResumeStream( ) override;
		void StopStream( ) override;
		bool IsStreaming( ) const override;
		int GetStreamVolume( ) const override;
		void SetStreamVolume( const int volume ) override;

	private:
		std::unordered_map<std::string, SoundStream*> m_pStreams;
		std::unordered_map<std::string, SoundEffect*> m_pEffects;
	};
}