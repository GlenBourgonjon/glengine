#pragma once

#include <vector>
#include <unordered_map>
#include <memory>
#include "Singleton.h"

namespace Glengine
{
	namespace Input
	{
		enum class ControllerButton
		{
			A = 0x1000,
			B = 0x2000,
			X = 0x4000,
			Y = 0x8000,
			Back = 0x0010,
			Start = 0x0020,
			LeftThumb = 0x0040,
			RightThumb = 0x0080,
			Up = 0x0001,
			Down = 0x0002,
			Left = 0x0004,
			Right = 0x0008,
			LeftTrigger = 0x0100,
			RightTrigger = 0x0200
		};
		enum class ButtonPressType
		{
			Down,
			Held,
			Up
		};
		enum class ControllerValue
		{
			ShoulderLeft = 0,
			ShoulderRight = 1,
			ThumbLeftX = 2,
			ThumbLeftY = 3,
			ThumbRightX = 4,
			ThumbRightY = 5,
			End = 6
		};
	}

	class Command;
	using namespace Input;

	class InputManager final : public Singleton<InputManager>
	{
	public:
		// Constructor / Destructor
		~InputManager( ) = default;

		// Input layouts
		void SetLayoutID( const int ID );
		int GetLayoutID( ) const;

		// General functionality
		bool ProcessInput( );
		void AddInputCommand( std::shared_ptr<Command> pCommand, const ControllerButton buttonType, const size_t controllerID, const ButtonPressType pressType = ButtonPressType::Down );
		void AddInputCommand( std::shared_ptr<Command> pCommand, const char button, const ButtonPressType pressType = ButtonPressType::Down );
		void RemoveAllCommands( );

		// Controller states
		bool IsControllerButtonDown( const ControllerButton button, size_t controllerID ) const;
		bool IsControllerButton( const ControllerButton button, size_t controllerID ) const;
		bool IsControllerButtonUp( const ControllerButton button, size_t controllerID ) const;
		float GetControllerValue( const ControllerValue valueType, size_t controllerID ) const;

		// Keyboard states
		bool IsButtonDown( const char button ) const;
		bool IsButton( const char button ) const;
		bool IsButtonUp( const char button ) const;

	private:
		// The struct that is used for commands coupled to specific inputs.
		struct InputCommand
		{
			union ButtonType
			{
				ControllerButton controller;
				char keyboard;
			};

			std::shared_ptr<Command> m_pCommand;
			ButtonType m_ButtonType;
			int m_ControllerID;
			ButtonPressType m_PressType;

			explicit InputCommand( std::shared_ptr<Command> pCommand, const ControllerButton buttonType, const int controllerID, const ButtonPressType pressType )
				: m_pCommand{ pCommand }
				, m_ControllerID{ controllerID }
				, m_PressType{ pressType }
			{
				m_ButtonType.controller = buttonType;
			}

			explicit InputCommand( std::shared_ptr<Command> pCommand, const char button, const ButtonPressType pressType )
				: m_pCommand{ pCommand }
				, m_ControllerID{ -1 }
				, m_PressType{ pressType }
			{
				m_ButtonType.keyboard = button;
			}
		};

		explicit InputManager( const size_t maxAmountOfControllers = 4 );

		int m_CurrentLayoutID;
		size_t m_MaxAmountOfControllers;
		std::vector<int> m_PreviousControllerStates;
		std::vector<int> m_CurrentControllerStates;
		std::vector<std::vector<int>> m_ControllerValues;

		short m_PreviousKeyboardState[256];
		short m_CurrentKeyboardState[256];

		std::unordered_map<int, std::vector<InputCommand>> m_InputCommands;

		friend class Singleton<InputManager>;
	};
}