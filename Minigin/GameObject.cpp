#include "MiniginPCH.h"
#include "GameObject.h"

namespace Glengine
{
	// Constructor and destructor

	GameObject::GameObject( )
		: m_Rotation{ 0.f }
		, m_Position{ 0.f, 0.f, 0.f }
		, m_Scale{ 1.f, 1.f, 1.f }
		, m_Tag{ "" }
	{}

	// Tag stuff

	const std::string& GameObject::GetTag( ) const
	{
		return m_Tag;
	}

	void GameObject::SetTag( const std::string& tag )
	{
		m_Tag = tag;
	}

	// Position stuff

	const glm::vec3& GameObject::GetPosition( ) const
	{
		return m_Position;
	}

	void GameObject::SetPosition( const float x, const float y, const float z )
	{
		m_Position.x = x;
		m_Position.y = y;
		m_Position.z = z;
	}

	void GameObject::SetPosition( const glm::vec3& position )
	{
		SetPosition(position.x, position.y, position.z);
	}

	void GameObject::SetPosition( const glm::vec2& position )
	{
		SetPosition(position.x, position.y);
	}

	// Rotation stuff

	float GameObject::GetRotation( ) const
	{
		return m_Rotation;
	}

	void GameObject::SetRotation( const float rotation )
	{
		m_Rotation = rotation;
	}

	// Scale stuff

	const glm::vec3& GameObject::GetScale( ) const
	{
		return m_Scale;
	}

	void GameObject::SetScale( const float x, const float y, const float z )
	{
		m_Scale.x = x;
		m_Scale.y = y;
		m_Scale.z = z;
	}

	void GameObject::SetScale( const glm::vec3& scale )
	{
		SetScale(scale.x, scale.y, scale.z);
	}

	void GameObject::SetScale( const glm::vec2& scale )
	{
		SetScale(scale.x, scale.y);
	}

	void GameObject::SetScale( const float scale )
	{
		SetScale(scale, scale, scale);
	}
}