#pragma once

#include "Minigin.h"
#include <iostream>

namespace Glengine
{
	class Time final
	{
	public:
		static double GetDeltaTime( )
		{
			return m_elapsedTime;
		};

		static int GetFPS( )
		{
			return m_FPS;
		};

		static float GetGameSpeed( )
		{
			return m_GameSpeed;
		}

		static void SetGameSpeed( const float gameSpeed)
		{
			m_GameSpeed = gameSpeed;
		}

	private:
		static void AddToFPSCount( const double elapsedTime )
		{
			m_AccumulatedTime += elapsedTime;
			++m_Frames;
			if (m_AccumulatedTime >= 1.0)
			{
				m_AccumulatedTime -= 1.0;
				m_FPS = m_Frames;
				m_Frames = 0;
			}
		}

		static void SetDeltaTime( const double elapsedTime )
		{
			m_elapsedTime = m_GameSpeed * elapsedTime;
		};

		static double m_elapsedTime;

		static double m_AccumulatedTime;
		static int m_Frames;
		static int m_FPS;
		static float m_GameSpeed;

		friend void Minigin::Run( );
	};
}