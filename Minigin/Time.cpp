#include "MiniginPCH.h"
#include "Time.h"

namespace Glengine
{
	double Time::m_elapsedTime = 0.0;
	double Time::m_AccumulatedTime = 0.0;
	int Time::m_Frames = 0;
	int Time::m_FPS = 0;
	float Time::m_GameSpeed = 1.f;
}