#include "MiniginPCH.h"
#include <algorithm>

#include <SDL_ttf.h>

#include "TextManager.h"
#include "GameObject.h"
#include "Renderer.h"
#include "Font.h"
#include "ResourceManager.h"

namespace Glengine
{
	TextComponent* TextManager::AddComponent( GameObject* pObject )
	{
		m_Components.push_back(TextComponent{ pObject });
		TextComponent* pComponent = GetComponent(pObject);
		return pComponent;
	}

	TextComponent* TextManager::GetComponent( GameObject* pObject )
	{
		auto it = std::find_if(m_Components.begin(), m_Components.end(), [pObject] (auto component)
			{
				return component.GetGameObject() == pObject;
			});

		if (it != m_Components.end())
			return &(*it);
		else
			return nullptr;
	}

	bool TextManager::RemoveComponent( GameObject* pObject )
	{
		auto it = std::find_if(m_Components.begin(), m_Components.end(), [pObject](auto component)
			{
				return component.GetGameObject() == pObject;
			});
		if (it != m_Components.end())
		{
			m_Components.erase(it);
			return true;
		}
		else
			return false;
	}

	void TextManager::RemoveAllComponents( )
	{
		m_Components.clear();
	}

	int TextManager::AddFont( const std::string& fileName, const int size )
	{
		m_pFonts.push_back(ResourceManager::GetInstance().LoadFont(fileName, size));
		return (int(m_pFonts.size()) - 1);
	}

	void TextManager::Update( )
	{
		for (TextComponent& component : m_Components)
		{
			const int id = component.m_FontID;
			if (component.m_NeedsUpdate && id >= 0 && id < int(m_pFonts.size()))
			{
				const auto surf = TTF_RenderText_Blended(m_pFonts[id]->GetFont(), component.m_Text.c_str(), component.m_Color);
				if (surf == nullptr)
				{
					throw std::runtime_error(std::string("Render text failed: ") + SDL_GetError());
				}

				auto texture = SDL_CreateTextureFromSurface(Renderer::GetInstance().GetSDLRenderer(), surf);
				if (texture == nullptr)
				{
					throw std::runtime_error(std::string("Create text texture from surface failed: ") + SDL_GetError());
				}

				SDL_FreeSurface(surf);
				component.m_pTexture = std::make_shared<Texture2D>( texture );
				component.m_NeedsUpdate = false;
			}
		}
	}

	void TextManager::Render( )
	{
		for (TextComponent& component : m_Components)
		{
			if (component.m_pTexture)
			{
				const glm::vec3& pos = component.GetGameObject()->GetPosition();
				Renderer::GetInstance().RenderTexture(*component.m_pTexture, pos.x, pos.y);
			}
		}
	}
}