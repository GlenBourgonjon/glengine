#include "MiniginPCH.h"
#include <algorithm>

#include "TextureManager.h"
#include "GameObject.h"
#include "Renderer.h"
#include "Texture2D.h"
#include "ResourceManager.h"
#include "Renderer.h"
#include "Time.h"

#include <SDL.h>

#pragma warning(push)
#pragma warning (disable:4201)
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#pragma warning(pop)

namespace Glengine
{
	TextureComponent* TextureManager::AddComponent( GameObject* pObject )
	{
		m_Components.push_back(TextureComponent{ pObject });
		TextureComponent* pComponent = GetComponent(pObject);
		return pComponent;
	}

	TextureComponent* TextureManager::GetComponent( GameObject* pObject )
	{
		auto it = std::find_if(m_Components.begin(), m_Components.end(), [pObject] (auto component)
			{
				return component.GetGameObject() == pObject;
			});
		if (it != m_Components.end())
			return &(*it);
		else
			return nullptr;
	}

	bool TextureManager::RemoveComponent( GameObject* pObject )
	{
		auto it = std::find_if(m_Components.begin(), m_Components.end(), [pObject](auto component)
			{
				return component.GetGameObject() == pObject;
			});

		if (it != m_Components.end())
		{
			m_Components.erase(it);
			return true;
		}
		else
			return false;
	}

	void TextureManager::RemoveAllComponents( )
	{
		m_Components.clear();
	}

	int TextureManager::AddTexture( const std::string& fileName, const int columns, const int rows )
	{
		m_pTextures.push_back(ResourceManager::GetInstance().LoadTexture(fileName, columns, rows));
		return (int(m_pTextures.size()) - 1);
	}

	void TextureManager::Update( )
	{
		const float deltaTime = float(Time::GetDeltaTime());

		std::sort(m_Components.begin(), m_Components.end(), [](auto component1, auto component2) { return component1.m_TextureID < component2.m_TextureID; });
		for (TextureComponent& component : m_Components)
		{
			const int id = component.m_TextureID;
			if (id >= 0 && id < int(m_pTextures.size()))
			{
				if (!component.m_Animated || component.m_MinSpriteID == -1 || component.m_MinSpriteID == component.m_MaxSpriteID)
					continue;

				component.m_CurrentAnimationTime += component.m_AnimationSpeed * deltaTime;
				const float totalTime = (component.m_MaxSpriteID - component.m_MinSpriteID) / (component.m_SpriteInterval * component.m_AnimationSpeed);
				if (component.m_CurrentAnimationTime < 0.f)
					component.m_CurrentAnimationTime += totalTime;
				else if (component.m_CurrentAnimationTime >= totalTime)
					component.m_CurrentAnimationTime -= totalTime;

				component.m_SpriteID = component.m_MinSpriteID + component.m_SpriteInterval * int(component.m_CurrentAnimationTime * (component.m_MaxSpriteID - component.m_MinSpriteID) / (component.m_SpriteInterval * totalTime));
			}
		}
	}

	void TextureManager::Render( )
	{
		for (const TextureComponent& component : m_Components)
		{
			const int id = component.m_TextureID;
			if (id >= 0 && id < int(m_pTextures.size()))
			{
				const glm::vec3& pos = component.GetGameObject()->GetPosition();
				const glm::vec3& scale = component.GetGameObject()->GetScale();

				int w, h;
				SDL_QueryTexture(m_pTextures[component.m_TextureID]->GetTexture(), nullptr, nullptr, &w, &h);
				const float width = float(w) / m_pTextures[component.m_TextureID]->GetColumns();
				const float height = float(h) / m_pTextures[component.m_TextureID]->GetRows();

				SDL_Rect src;
				src.x = static_cast<int>(width * (component.m_SpriteID % m_pTextures[component.m_TextureID]->GetColumns()));
				src.y = static_cast<int>(height * (component.m_SpriteID / m_pTextures[component.m_TextureID]->GetColumns()));
				src.w = static_cast<int>(width);
				src.h = static_cast<int>(height);
				SDL_Rect dest;
				dest.x = static_cast<int>(pos.x - component.m_Pivot.x * width * scale.x);
				dest.y = static_cast<int>(pos.y - component.m_Pivot.y * height * scale.y);
				dest.w = static_cast<int>(width * scale.x);
				dest.h = static_cast<int>(height * scale.y);
				
				Renderer::GetInstance().RenderTexture(*m_pTextures[component.m_TextureID], dest, src, int(component.GetGameObject()->GetRotation()), component.m_Flipped);
			}
		}
	}
}