#pragma once

#include <SDL_mixer.h>

namespace Glengine
{
	class SoundStream final
	{
	public:
		explicit SoundStream( const std::string& path );
		~SoundStream( );
		SoundStream( const SoundStream& other ) = delete;
		SoundStream& operator=( const SoundStream& other ) = delete;
		SoundStream( SoundStream&& other ) = delete;
		SoundStream& operator=( SoundStream&& other ) = delete;

		bool IsLoaded( ) const;
		bool Play( bool repeat ) const;

	private:
		Mix_Music* m_pMixMusic;
	};
}