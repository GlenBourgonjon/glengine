// Based on implementation in Elite Engine by Matthieu Delaere
#pragma once

#include <functional>
#include <vector>

namespace Glengine
{
	class GameObject;

	enum class BehaviorState
	{
		Failure,
		Success,
		Running
	};

	class IBehavior
	{
	public:
		IBehavior( ) = default;
		virtual ~IBehavior( ) = default;
		virtual BehaviorState Execute( GameObject* pObject ) const = 0;
	};

	class BehaviorComposite : public IBehavior
	{
	public:
		explicit BehaviorComposite( std::vector<IBehavior*> childrenBehaviors )
			: m_ChildrenBehaviors{ childrenBehaviors }
		{}
		
		virtual ~BehaviorComposite( )
		{
			for (auto pb : m_ChildrenBehaviors)
			{
				delete pb;
			}
			m_ChildrenBehaviors.clear();
		}

		virtual BehaviorState Execute( GameObject* pObject ) const override = 0;

	protected:
		std::vector<IBehavior*> m_ChildrenBehaviors;
	};

	class BehaviorSelector : public BehaviorComposite
	{
	public:
		explicit BehaviorSelector( std::vector<IBehavior*> childrenBehaviors )
			: BehaviorComposite{ childrenBehaviors }
		{}
		
		virtual ~BehaviorSelector( ) = default;

		virtual BehaviorState Execute( GameObject* pObject ) const override;
	};

	class BehaviorSequence : public BehaviorComposite
	{
	public:
		explicit BehaviorSequence( std::vector<IBehavior*> childrenBehaviors )
			: BehaviorComposite{ childrenBehaviors }
		{}
		
		virtual ~BehaviorSequence( ) = default;

		virtual BehaviorState Execute( GameObject* pObject ) const override;
	};

	class BehaviorCollection : public BehaviorComposite
	{
	public:
		explicit BehaviorCollection( std::vector<IBehavior*> childrenBehaviors )
			: BehaviorComposite{ childrenBehaviors }
		{}
		
		virtual ~BehaviorCollection( ) = default;

		virtual BehaviorState Execute( GameObject* pObject ) const override;
	};

	class BehaviorConditional : public IBehavior
	{
	public:
		explicit BehaviorConditional( std::function<bool(GameObject*)> fp )
			: m_fpConditional(fp)
		{}
		
		virtual BehaviorState Execute( GameObject* pObject ) const override;

	private:
		std::function<bool(GameObject*)> m_fpConditional = nullptr;
	};

	class BehaviorAction : public IBehavior
	{
	public:
		explicit BehaviorAction( std::function<BehaviorState(GameObject*)> fp )
			: m_fpAction(fp)
		{}

		virtual BehaviorState Execute( GameObject* pObject ) const override;

	private:
		std::function<BehaviorState(GameObject*)> m_fpAction = nullptr;
	};

	class BehaviorTree final
	{
	public:
		explicit BehaviorTree( IBehavior* pRootComposite )
			: m_pRootComposite{ pRootComposite }
		{}

		~BehaviorTree( )
		{
			if (m_pRootComposite != nullptr)
				delete m_pRootComposite;
		};

		BehaviorState Update( GameObject* pObject ) const
		{
			if (m_pRootComposite != nullptr)
				return m_pRootComposite->Execute(pObject);
			else
				return BehaviorState::Failure;
		}

	private:
		IBehavior* m_pRootComposite;
	};
}