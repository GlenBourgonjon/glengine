#pragma once

#include <unordered_map>
#include <functional>

#include "Singleton.h"
#include "Component.h"

namespace Glengine
{
	typedef std::function<void(GameObject* pObject)> TimerCallback;

	class TimerComponent final : public Component
	{
	public:
		bool Start( const float countdown, TimerCallback callback, const bool repeat = false )
		{
			if (m_Started)
				return false;

			m_Repeat = repeat;
			m_Callback = callback;
			m_MaxCountdown = countdown;
			m_Countdown = countdown;
			m_Started = true;
			return true;
		}

	private:
		explicit TimerComponent( GameObject* pObject )
			: Component{ pObject }
			, m_Started{ false }
			, m_Repeat{ false }
			, m_Countdown{ 0.f }
			, m_MaxCountdown{ 0.f }
			, m_Callback{ nullptr }
		{}

		bool m_Started;
		bool m_Repeat;
		float m_Countdown;
		float m_MaxCountdown;
		TimerCallback m_Callback;

		friend class TimerManager;
	};

	class TimerManager final : public Singleton<TimerManager>
	{
	public:
		TimerComponent* AddComponent( GameObject* pObject );
		TimerComponent* GetComponent( GameObject* pObject );
		bool RemoveComponent( GameObject* pObject );
		void RemoveAllComponents( );

	private:
		TimerManager( ) = default;

		void Update( );

		std::unordered_map<GameObject*, TimerComponent> m_Components;

		friend class Singleton<TimerManager>;
		friend class Scene;
	};
}