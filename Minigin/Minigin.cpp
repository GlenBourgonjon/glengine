#include "MiniginPCH.h"

#include "Minigin.h"
#include <chrono>
#include <thread>
#include "Input.h"
#include "SceneManager.h"
#include "Renderer.h"
#include "ResourceManager.h"
#include <SDL.h>
#include <SDL_mixer.h>
#include "Time.h"

namespace Glengine
{
	void Minigin::Initialize( const char* windowTitle, const int width, const int height )
	{
		if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
			throw std::runtime_error(std::string("SDL_Init Error: ") + SDL_GetError());

		m_Window = SDL_CreateWindow
		(
			windowTitle,
			SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED,
			width,
			height,
			SDL_WINDOW_OPENGL
		);

		if (m_Window == nullptr)
			throw std::runtime_error(std::string("SDL_CreateWindow Error: ") + SDL_GetError());

		if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
		{
			std::cerr << "Core::Initialize( ), error when calling Mix_OpenAudio: " << Mix_GetError() << std::endl;
			return;
		}

		Renderer::GetInstance().Init(m_Window);
		ResourceManager::GetInstance().Init("Resources/Textures/");
	}

	void Minigin::Cleanup( )
	{
		Renderer::GetInstance().Destroy();
		SDL_DestroyWindow(m_Window);
		m_Window = nullptr;
		SDL_Quit();
	}

	void Minigin::Run( )
	{
		auto& renderer = Renderer::GetInstance();
		auto& sceneManager = SceneManager::GetInstance();

		// Game loop.
		const float maxElapsedTime{ 0.1f };
		bool doContinue = true;
		auto lastTime = std::chrono::high_resolution_clock::now();
		while (doContinue)
		{
			// Updating the time.
			const auto currentTime = std::chrono::high_resolution_clock::now();
			const float deltaTime = std::chrono::duration<float>(currentTime - lastTime).count();
			Time::AddToFPSCount(deltaTime);
			Time::SetDeltaTime(min(deltaTime, maxElapsedTime));
			lastTime = currentTime;

			// Input processing.
			doContinue = InputManager::GetInstance().ProcessInput();
			if (!doContinue)
				sceneManager.SetActiveScene(-1);

			// Updates.
			sceneManager.Update();
			renderer.Render();
		}

		Cleanup();
	}
}