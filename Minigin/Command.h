#pragma once

namespace Glengine
{
	class Command
	{
	public:
		virtual ~Command( ) = default;

		virtual void Execute( ) = 0;
	};
}