#pragma once

#include <vector>
#include <memory>
#include <SDL.h>

#include "Singleton.h"
#include "Component.h"
#include "Texture2D.h"

namespace Glengine
{
	class Font;

	class TextComponent final : public Component
	{
	public:
		void SetText( const std::string& text )
		{
			m_Text = text;
			m_NeedsUpdate = true;
		}

		void SetColor( const SDL_Color& color )
		{
			m_Color = color;
			m_NeedsUpdate = true;
		}

		void SetFont( const int fontID )
		{
			m_FontID = fontID;
			m_NeedsUpdate = true;
		}

	private:
		explicit TextComponent( GameObject* pObject )
			: Component{ pObject }
			, m_NeedsUpdate{ false }
			, m_FontID{ -1 }
			, m_pTexture{ nullptr }
			, m_Color{ }
			, m_Text{ "" }
		{}

		bool m_NeedsUpdate;
		int m_FontID;
		std::shared_ptr<Texture2D> m_pTexture;
		SDL_Color m_Color;
		std::string m_Text;

		friend class TextManager;
	};

	class TextManager final : public Singleton<TextManager>
	{
	public:
		TextComponent* AddComponent( GameObject* pObject );
		TextComponent* GetComponent( GameObject* pObject );
		bool RemoveComponent( GameObject* pObject );
		void RemoveAllComponents( );

		int AddFont( const std::string& fileName, const int size );

	private:
		TextManager( ) = default;

		void Update( );
		void Render( );

		std::vector<TextComponent> m_Components;
		std::vector<std::shared_ptr<Font>> m_pFonts;

		friend class Singleton<TextManager>;
		friend class Scene;
	};
}