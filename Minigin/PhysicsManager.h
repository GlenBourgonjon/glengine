#pragma once

#include <unordered_map>
#include <functional>

#pragma warning(push)
#pragma warning (disable:4201)
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#pragma warning(pop)

#include "Singleton.h"
#include "Component.h"

namespace Glengine
{
	struct Rectangle final
	{
		float left;
		float bottom;
		float right;
		float top;
	};

	typedef std::function<void(GameObject* pObject1, GameObject* pObject2)> PhysicsCallback;

	class PhysicsComponent final : public Component
	{
	public:
		unsigned char GetLayer( ) const
		{
			return m_Layer;
		}

		void SetLayer( const unsigned char layer )
		{
			m_Layer = layer;
		}
		
		void SetCollisionFlags( const unsigned char flags )
		{
			m_CollisionFlags = flags;
		}

		bool IsTrigger( ) const
		{
			return m_Trigger;
		}

		void SetTrigger( const bool value )
		{
			m_Trigger = value;
		}

		bool IsStatic( ) const
		{
			return m_Static;
		}

		void SetStatic( const bool value )
		{
			m_Static = value;
			if (m_Static)
				m_Moving = false;
		}

		bool IsGrounded( ) const
		{
			return m_Grounded;
		}

		bool IsMoving( ) const
		{
			return m_Moving;
		}

		void SetBounce( const float value )
		{
			m_Bounce = value;
			if (m_Bounce < 0.f)
				m_Bounce = 0.f;
			else if (m_Bounce > 1.f)
				m_Bounce = 1.f;
		}

		int GetHorizontalDirection( ) const
		{
			return m_HorizontalDirection;
		}

		int GetVerticalDirection( ) const
		{
			return m_VerticalDirection;
		}

		void SetCollisionCallback( PhysicsCallback callback )
		{
			m_CollisionCallback = callback;
		}

		void SetTriggerCallback( PhysicsCallback callback )
		{
			m_TriggerCallback = callback;
		}

		void SetFriction( const float value )
		{
			m_Friction = value;
			if (m_Friction < 0.f)
				m_Friction = 0.f;
			else if (m_Friction > 1.f)
				m_Friction = 1.f;
		}

		const glm::vec2& GetGravity( ) const
		{
			return m_Gravity;
		}

		void SetGravity( const glm::vec2& gravity )
		{
			m_Gravity = gravity;
		}

		const glm::vec2& GetVelocity( ) const
		{
			return m_Velocity;
		}

		void SetVelocity( const glm::vec2& velocity )
		{
			m_Velocity = velocity;
		}

		void SetVelocityX( const float x )
		{
			m_Velocity.x = x;
		}

		void SetVelocityY( const float y )
		{
			m_Velocity.y = y;
		}

		void SetBoundingBox( const Rectangle& boundingBox )
		{
			m_BoundingBox = boundingBox;
		}

	private:
		explicit PhysicsComponent( GameObject* pObject )
			: Component{ pObject }
			, m_Layer{ 1 }
			, m_CollisionFlags{ 255 }
			, m_Trigger{ false }
			, m_Static{ true }
			, m_Grounded{ false }
			, m_Moving{ false }
			, m_Friction{ 0.f }
			, m_Bounce{ 0.f }
			, m_HorizontalDirection{ 0 }
			, m_VerticalDirection{ 0 }
			, m_CollisionCallback{ nullptr }
			, m_TriggerCallback{ nullptr }
			, m_Gravity{ 0.f, 0.f }
			, m_Velocity{ 0.f, 0.f }
			, m_BoundingBox{ 0.f, 0.f, 0.f, 0.f }
			, m_pCurrentColliders{ }
			, m_pCurrentTriggers{ }
		{}

		unsigned char m_Layer;
		unsigned char m_CollisionFlags;

		bool m_Trigger;
		bool m_Static;
		bool m_Grounded;
		bool m_Moving;

		float m_Friction;
		float m_Bounce;

		int m_HorizontalDirection;
		int m_VerticalDirection;

		PhysicsCallback m_CollisionCallback;
		PhysicsCallback m_TriggerCallback;

		glm::vec2 m_Gravity;
		glm::vec2 m_Velocity;
		Rectangle m_BoundingBox;

		std::vector<GameObject*> m_pCurrentColliders;
		std::vector<GameObject*> m_pCurrentTriggers;

		friend class PhysicsManager;
	};

	class PhysicsManager final : public Singleton<PhysicsManager>
	{
	public:
		PhysicsComponent* AddComponent( GameObject* pObject );
		PhysicsComponent* GetComponent( GameObject* pObject );
		bool RemoveComponent( GameObject* pObject );
		void RemoveAllComponents( );

	private:
		PhysicsManager( ) = default;

		void Update( );
		void Move( std::pair<GameObject* const, PhysicsComponent>& componentPair );
		bool CheckCollision( std::pair<GameObject* const, PhysicsComponent>& pair1, std::pair<GameObject* const, PhysicsComponent>& pair2, glm::vec2& velocity );
		bool CheckTrigger( std::pair<GameObject* const, PhysicsComponent>& pair1, std::pair<GameObject* const, PhysicsComponent>& pair2 );
		
		std::unordered_map<GameObject*, PhysicsComponent> m_Components;

		friend class Singleton<PhysicsManager>;
		friend class Scene;
	};
}