#pragma once

#include "Singleton.h"
#include <vector>

namespace Glengine
{
	class Scene;

	class SceneManager final : public Singleton<SceneManager>
	{
	public:
		~SceneManager( );

		Scene* AddScene( Scene* pNewScene );
		void SetActiveScene( const Scene* pScene );
		void SetActiveScene( const int index );
		Scene* GetActiveScene( ) const;
		int GetActiveSceneIndex( ) const;
		Scene* GetScene( const int index );

		void Update( );
		void Render( );

	private:
		SceneManager( );

		std::vector<Scene*> m_pScenes;
		int m_CurrentScene;
		int m_NextSceme;

		friend class Singleton<SceneManager>;
	};
}
