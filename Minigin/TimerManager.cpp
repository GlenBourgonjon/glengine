#include "MiniginPCH.h"
#include "TimerManager.h"
#include "Time.h"

namespace Glengine
{
	TimerComponent* TimerManager::AddComponent( GameObject* pObject )
	{
		if (m_Components.count(pObject) == 0)
		{
			m_Components.emplace(std::make_pair(pObject, TimerComponent{ pObject }));
			TimerComponent* pComponent = &m_Components.at(pObject);
			return pComponent;
		}
		else
			return nullptr;
	}

	TimerComponent* TimerManager::GetComponent( GameObject* pObject )
	{
		if (m_Components.count(pObject) > 0)
			return &m_Components.at(pObject);
		else
			return nullptr;
	}

	bool TimerManager::RemoveComponent( GameObject* pObject )
	{
		return (m_Components.erase(pObject) > 0);
	}

	void TimerManager::RemoveAllComponents( )
	{
		m_Components.clear();
	}

	void TimerManager::Update( )
	{
		const float elapsedSec = float(Time::GetDeltaTime());
		std::vector<GameObject*> pToDelete{};

		for (std::pair<GameObject* const, TimerComponent>& component : m_Components)
		{
			if (!component.second.m_Started)
				continue;

			component.second.m_Countdown -= elapsedSec;
			if (component.second.m_Countdown <= 0.f)
			{
				if (component.second.m_Callback != nullptr)
					component.second.m_Callback(component.first);

				if (!component.second.m_Repeat)
					pToDelete.push_back(component.first);
				else
					component.second.m_Countdown = component.second.m_MaxCountdown;
			}
		}

		for (GameObject* pObject : pToDelete)
		{
			m_Components.erase(pObject);
		}
	}
}