#include "MiniginPCH.h"
#include "Input.h"
#include "windows.h"
#include "xinput.h"
#include "Command.h"
#include "SDL.h"

namespace Glengine
{
	InputManager::InputManager( const size_t maxAmountOfControllers )
		: m_MaxAmountOfControllers{ (maxAmountOfControllers < 5) ? maxAmountOfControllers : 4 }
		, m_CurrentLayoutID{ 0 }
	{
		m_PreviousControllerStates = std::vector<int>(m_MaxAmountOfControllers, 0);
		m_CurrentControllerStates = std::vector<int>(m_MaxAmountOfControllers, 0);
		m_ControllerValues = std::vector<std::vector<int>>(m_MaxAmountOfControllers, std::vector<int>(int(ControllerValue::End), 0));
	}

	void InputManager::SetLayoutID( const int ID )
	{
		m_CurrentLayoutID = ID;
	}
	
	int InputManager::GetLayoutID( ) const
	{
		return m_CurrentLayoutID;
	}

	bool InputManager::ProcessInput( )
	{
		SDL_Event e;
		while (SDL_PollEvent(&e))
		{
			if (e.type == SDL_QUIT)
				return false;
		}

		// Process controller input.
		XINPUT_STATE controllerState;
		for (int i = 0; i < int(m_MaxAmountOfControllers); ++i)
		{
			ZeroMemory(&controllerState, sizeof(XINPUT_STATE));
			if (XInputGetState(i, &controllerState) == ERROR_SUCCESS)
			{
				m_PreviousControllerStates[i] = m_CurrentControllerStates[i];
				m_CurrentControllerStates[i] = controllerState.Gamepad.wButtons;
				m_ControllerValues[i][int(ControllerValue::ShoulderLeft)] = controllerState.Gamepad.bLeftTrigger;
				m_ControllerValues[i][int(ControllerValue::ShoulderRight)] = controllerState.Gamepad.bRightTrigger;
				m_ControllerValues[i][int(ControllerValue::ThumbLeftX)] = controllerState.Gamepad.sThumbLX;
				m_ControllerValues[i][int(ControllerValue::ThumbLeftY)] = controllerState.Gamepad.sThumbLY;
				m_ControllerValues[i][int(ControllerValue::ThumbRightX)] = controllerState.Gamepad.sThumbRX;
				m_ControllerValues[i][int(ControllerValue::ThumbRightY)] = controllerState.Gamepad.sThumbRY;
			}
		}

		// Process keyboard input.
		memcpy(&m_PreviousKeyboardState, &m_CurrentKeyboardState, sizeof(short[256]));
		for (int i = 0; i < 256; ++i)
		{
			m_CurrentKeyboardState[i] = GetAsyncKeyState(i);
		}

		// Execute commands if necessary.
		const int id = m_CurrentLayoutID;
		for (InputCommand& command : m_InputCommands[id])
		{
			if (command.m_ControllerID >= 0)
			{
				switch (command.m_PressType)
				{
				case ButtonPressType::Down:
					if (IsControllerButtonDown(command.m_ButtonType.controller, command.m_ControllerID))
						command.m_pCommand->Execute();
					break;
				case ButtonPressType::Held:
					if (IsControllerButton(command.m_ButtonType.controller, command.m_ControllerID))
						command.m_pCommand->Execute();
					break;
				case ButtonPressType::Up:
					if (IsControllerButtonUp(command.m_ButtonType.controller, command.m_ControllerID))
						command.m_pCommand->Execute();
					break;
				}
			}
			else
			{
				switch (command.m_PressType)
				{
				case ButtonPressType::Down:
					if (IsButtonDown(command.m_ButtonType.keyboard))
						command.m_pCommand->Execute();
					break;
				case ButtonPressType::Held:
					if (IsButton(command.m_ButtonType.keyboard))
						command.m_pCommand->Execute();
					break;
				case ButtonPressType::Up:
					if (IsButtonUp(command.m_ButtonType.keyboard))
						command.m_pCommand->Execute();
					break;
				}
			}
		}

		// Check if the game needs to stop.
		return !m_CurrentKeyboardState[27];
	}

	void InputManager::AddInputCommand( std::shared_ptr<Command> pCommand, const ControllerButton buttonType, const size_t controllerID, const ButtonPressType pressType )
	{
		int id = int(min(controllerID, m_MaxAmountOfControllers));
		m_InputCommands[m_CurrentLayoutID].push_back(InputCommand{ pCommand, buttonType, id, pressType });
	}

	void InputManager::AddInputCommand( std::shared_ptr<Command> pCommand, const char button, const ButtonPressType pressType )
	{
		m_InputCommands[m_CurrentLayoutID].push_back(InputCommand{ pCommand, button, pressType });
	}

	void InputManager::RemoveAllCommands( )
	{
		m_InputCommands.clear();
		m_CurrentLayoutID = 0;
	}

	bool InputManager::IsControllerButtonDown( const ControllerButton button, size_t controllerID ) const
	{
		controllerID = min(controllerID, m_MaxAmountOfControllers);
		return	((m_CurrentControllerStates[controllerID] & int(button)) != 0)
				&& ((m_PreviousControllerStates[controllerID] & int(button)) == 0);
	}

	bool InputManager::IsControllerButton( const ControllerButton button, size_t controllerID ) const
	{
		controllerID = min(controllerID, m_MaxAmountOfControllers);
		return	(m_CurrentControllerStates[controllerID] & int(button)) != 0;
	}

	bool InputManager::IsControllerButtonUp( const ControllerButton button, size_t controllerID ) const
	{
		controllerID = min(controllerID, m_MaxAmountOfControllers);
		return	((m_CurrentControllerStates[controllerID] & int(button)) == 0)
				&& ((m_PreviousControllerStates[controllerID] & int(button)) != 0);
	}

	float InputManager::GetControllerValue( const ControllerValue valueType, size_t controllerID ) const
	{
		controllerID = min(controllerID, m_MaxAmountOfControllers);
		const int value = m_ControllerValues[controllerID][int(valueType)];
		const float maxThumbValue = 32767.f;
		const float maxShoulderValue = 255.f;

		switch (valueType)
		{
		case ControllerValue::ThumbLeftX:
		case ControllerValue::ThumbLeftY:
			if (value < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE && value > -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
				return 0.f;
			else
				return value / maxThumbValue;
		case ControllerValue::ThumbRightX:
		case ControllerValue::ThumbRightY:
			if (value < XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE && value > -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE)
				return 0.f;
			else
				return value / maxThumbValue;
		default:
			return value / maxShoulderValue;
		}
	}

	bool InputManager::IsButtonDown( const char button ) const
	{
		return	m_CurrentKeyboardState[button] != 0
				&& m_PreviousKeyboardState[button] == 0;
	}
	
	bool InputManager::IsButton( const char button ) const
	{
		return	m_CurrentKeyboardState[button] != 0;
	}
	
	bool InputManager::IsButtonUp( const char button ) const
	{
		return	m_CurrentKeyboardState[button] == 0
				&& m_PreviousKeyboardState[button] != 0;
	}
}