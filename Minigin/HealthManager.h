#pragma once

#include <unordered_map>
#include <functional>

#include "Singleton.h"
#include "Component.h"

namespace Glengine
{
	class HealthComponent final : public Component
	{
	public:
		bool IsDead( ) const
		{
			return m_IsDead;
		}

		void ChangeHealth( const int change )
		{
			m_NewHealth = m_Health + change;
		}

		void SetHealth( const int health )
		{
			m_Health = health;
			m_NewHealth = health;
		}

		int GetHealth( ) const
		{
			return m_NewHealth;
		}

	private:
		explicit HealthComponent( GameObject* pObject )
			: Component{ pObject }
			, m_IsDead{ false }
			, m_Health{ 0 }
			, m_NewHealth{ 0 }
		{}

		bool m_IsDead;
		int m_Health;
		int m_NewHealth;

		friend class HealthManager;
	};

	class HealthManager final : public Singleton<HealthManager>
	{
	public:
		HealthComponent* AddComponent( GameObject* pObject );
		HealthComponent* GetComponent( GameObject* pObject );
		bool RemoveComponent( GameObject* pObject );
		void RemoveAllComponents( );

	private:
		HealthManager( ) = default;

		void Update( );

		std::unordered_map<GameObject*, HealthComponent> m_Components;

		friend class Singleton<HealthManager>;
		friend class Scene;
	};
}