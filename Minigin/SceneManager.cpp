#include "MiniginPCH.h"

#include "SceneManager.h"
#include "Scene.h"

namespace Glengine
{
	SceneManager::SceneManager( )
		: m_CurrentScene{ -1 }
		, m_NextSceme{ -1 }
	{}

	SceneManager::~SceneManager( )
	{
		for (Scene* pScene : m_pScenes)
		{
			delete pScene;
		}
	}

	Scene* SceneManager::AddScene( Scene* pNewScene )
	{
		m_pScenes.push_back(pNewScene);
		return pNewScene;
	}

	void SceneManager::SetActiveScene( const Scene* pScene )
	{
		auto it = std::find(m_pScenes.cbegin(), m_pScenes.cend(), pScene);
		if (it != m_pScenes.cend())
			SetActiveScene(int(std::distance(m_pScenes.cbegin(), it)));
	}
	
	void SceneManager::SetActiveScene( const int index )
	{
		if (index == m_CurrentScene || index >= int(m_pScenes.size()))
			return;

		m_NextSceme = index;
	}

	Scene* SceneManager::GetActiveScene( ) const
	{
		if (m_CurrentScene >= 0 && m_CurrentScene < int(m_pScenes.size()))
			return m_pScenes[m_CurrentScene];
		else
			return nullptr;
	}

	int SceneManager::GetActiveSceneIndex( ) const
	{
		return m_CurrentScene;
	}

	Scene* SceneManager::GetScene( const int index )
	{
		if (index < 0 || index >= int(m_pScenes.size()))
			return nullptr;

		return m_pScenes[index];
	}

	void SceneManager::Update( )
	{
		if (m_NextSceme >= 0 && m_NextSceme != m_CurrentScene)
		{
			// Deactivating the old current scene.
			if (m_CurrentScene >= 0 && m_CurrentScene < int(m_pScenes.size()))
				m_pScenes[m_CurrentScene]->Deactivate();

			// Activating the new current scene.
			m_CurrentScene = m_NextSceme;
			m_pScenes[m_CurrentScene]->Activate();
		}
		else
		{
			m_CurrentScene = m_NextSceme;

			if (m_CurrentScene >= 0)
				m_pScenes[m_CurrentScene]->Update();
		}
	}

	void SceneManager::Render( )
	{
		if (m_CurrentScene >= 0)
			m_pScenes[m_CurrentScene]->Render();
	}
}