#include "MiniginPCH.h"

#include "PhysicsManager.h"
#include "GameObject.h"
#include "Time.h"

namespace Glengine
{
	PhysicsComponent* PhysicsManager::AddComponent( GameObject* pObject )
	{
		if (m_Components.count(pObject) == 0)
		{
			m_Components.emplace(std::make_pair(pObject, PhysicsComponent{ pObject }));
			PhysicsComponent* pComponent = &m_Components.at(pObject);
			return pComponent;
		}
		else
			return nullptr;
	}

	PhysicsComponent* PhysicsManager::GetComponent( GameObject* pObject )
	{
		if (m_Components.count(pObject) > 0)
			return &m_Components.at(pObject);
		else
			return nullptr;
	}

	bool PhysicsManager::RemoveComponent( GameObject* pObject )
	{
		return (m_Components.erase(pObject) > 0);
	}

	void PhysicsManager::RemoveAllComponents( )
	{
		m_Components.clear();
	}

	void PhysicsManager::Update( )
	{
		const float elapsedTime = float(Time::GetDeltaTime());
		const float gravityForce = 65.f;

		for (std::pair<GameObject* const, PhysicsComponent>& component : m_Components)
		{
			if (component.second.m_Static)
				continue;

			component.second.m_Velocity.x += gravityForce * elapsedTime * component.second.m_Gravity.x;
			component.second.m_Velocity.y += gravityForce * elapsedTime * component.second.m_Gravity.y;

			Move(component);
		}
	}

	void PhysicsManager::Move( std::pair<GameObject* const, PhysicsComponent>& componentPair )
	{
		PhysicsComponent& component = componentPair.second;
		glm::vec2 velocity = component.m_Velocity * float(Time::GetDeltaTime());
		component.m_Grounded = false;

		// Update directions and moving variable.
		component.m_Moving = true;
		if (velocity.x > FLT_EPSILON)
			component.m_HorizontalDirection = 1;
		else if (velocity.x < -FLT_EPSILON)
			component.m_HorizontalDirection = -1;
		else
			component.m_Moving = false;
		if (velocity.y > FLT_EPSILON)
			component.m_VerticalDirection = 1;
		else if (velocity.y < -FLT_EPSILON)
			component.m_VerticalDirection = -1;

		// Check collisions.
		std::vector<PhysicsComponent*> colliders{ };
		std::vector<PhysicsComponent*> triggers{ };
		for (std::pair<GameObject* const, PhysicsComponent>& other : m_Components)
		{
			if (other.first == componentPair.first)
				continue;
			else if (!component.m_Trigger && other.second.m_Trigger)
			{
				if (CheckTrigger(componentPair, other))
					triggers.push_back(&other.second);
			}
			else if ((other.second.m_Layer & component.m_CollisionFlags) == 0)
				continue;
			else if (CheckCollision(componentPair, other, velocity))
				colliders.push_back(&other.second);
		}

		// Move.
		glm::vec3 position = componentPair.first->GetPosition();
		position += glm::vec3{ velocity.x, velocity.y, 0.f };
		componentPair.first->SetPosition(position);

		// Friction?
		component.SetVelocityX(powf((1 - component.m_Friction), float(Time::GetDeltaTime())) * component.m_Velocity.x);

		// Collision callbacks.
		for (PhysicsComponent* collider : colliders)
		{
			if (std::find(component.m_pCurrentColliders.cbegin(), component.m_pCurrentColliders.cend(), collider->GetGameObject()) == component.m_pCurrentColliders.cend())
			{
				if (component.m_CollisionCallback != nullptr)
					component.m_CollisionCallback(component.GetGameObject(), collider->GetGameObject());
			}
		}
		component.m_pCurrentColliders.clear();
		for (PhysicsComponent* collider : colliders)
		{
			component.m_pCurrentColliders.push_back(collider->GetGameObject());
		}

		// Trigger callbacks.
		for (PhysicsComponent* trigger : triggers)
		{
			if (std::find(component.m_pCurrentTriggers.cbegin(), component.m_pCurrentTriggers.cend(), trigger->GetGameObject()) == component.m_pCurrentTriggers.cend())
			{
				if (component.m_TriggerCallback != nullptr)
					component.m_TriggerCallback(component.GetGameObject(), trigger->GetGameObject());
			}
		}
		component.m_pCurrentTriggers.clear();
		for (PhysicsComponent* trigger : triggers)
		{
			component.m_pCurrentTriggers.push_back(trigger->GetGameObject());
		}
	}

	bool PhysicsManager::CheckCollision( std::pair<GameObject* const, PhysicsComponent>& pair1, std::pair<GameObject* const, PhysicsComponent>& pair2, glm::vec2& velocity )
	{
		const glm::vec3& pos = pair1.first->GetPosition();
		Rectangle newBoundingBox = pair1.second.m_BoundingBox;
		newBoundingBox.left += pos.x;
		newBoundingBox.right += pos.x;
		newBoundingBox.bottom += pos.y;
		newBoundingBox.top += pos.y;

		const glm::vec3& otherPos = pair2.first->GetPosition();
		Rectangle otherBoundingBox = pair2.second.m_BoundingBox;
		otherBoundingBox.left += otherPos.x;
		otherBoundingBox.right += otherPos.x;
		otherBoundingBox.bottom += otherPos.y;
		otherBoundingBox.top += otherPos.y;

		if (newBoundingBox.right + velocity.x < otherBoundingBox.left || otherBoundingBox.right < newBoundingBox.left + velocity.x)
			return false;
		else if (newBoundingBox.bottom + velocity.y > otherBoundingBox.top || otherBoundingBox.bottom > newBoundingBox.top + velocity.y)
			return false;

		if (velocity.x > FLT_EPSILON && newBoundingBox.right <= otherBoundingBox.left && newBoundingBox.right + velocity.x >= otherBoundingBox.left)
		{
			velocity.x = otherBoundingBox.left - newBoundingBox.right - 0.1f;
			pair1.second.m_Velocity.x = 0.f;
		}
		else if (velocity.x < -FLT_EPSILON && newBoundingBox.left >= otherBoundingBox.right && newBoundingBox.left + velocity.x <= otherBoundingBox.right)
		{
			velocity.x = otherBoundingBox.right - newBoundingBox.left + 0.1f;
			pair1.second.m_Velocity.x = 0.f;
		}

		if (newBoundingBox.right + velocity.x < otherBoundingBox.left || otherBoundingBox.right < newBoundingBox.left + velocity.x)
			return true;
		else if (newBoundingBox.bottom + velocity.y > otherBoundingBox.top || otherBoundingBox.bottom > newBoundingBox.top + velocity.y)
			return true;

		if (velocity.y >= 0 && newBoundingBox.top <= otherBoundingBox.bottom && newBoundingBox.top + velocity.y >= otherBoundingBox.bottom)
		{
			velocity.y = otherBoundingBox.bottom - newBoundingBox.top - 0.1f;
			pair1.second.m_Velocity.y *= -pair1.second.m_Bounce;
			pair1.second.m_Grounded = true;
		}

		return true;
	}

	bool PhysicsManager::CheckTrigger( std::pair<GameObject* const, PhysicsComponent>& pair1, std::pair<GameObject* const, PhysicsComponent>& pair2 )
	{
		const glm::vec3& pos = pair1.first->GetPosition();
		Rectangle newBoundingBox = pair1.second.m_BoundingBox;
		newBoundingBox.left += pos.x;
		newBoundingBox.right += pos.x;
		newBoundingBox.bottom += pos.y;
		newBoundingBox.top += pos.y;

		const glm::vec3& otherPos = pair2.first->GetPosition();
		Rectangle otherBoundingBox = pair2.second.m_BoundingBox;
		otherBoundingBox.left += otherPos.x;
		otherBoundingBox.right += otherPos.x;
		otherBoundingBox.bottom += otherPos.y;
		otherBoundingBox.top += otherPos.y;

		if (newBoundingBox.right < otherBoundingBox.left || otherBoundingBox.right < newBoundingBox.left)
			return false;
		else if (newBoundingBox.bottom > otherBoundingBox.top || otherBoundingBox.bottom > newBoundingBox.top)
			return false;

		return true;
	}
}