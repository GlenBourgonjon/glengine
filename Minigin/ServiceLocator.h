#pragma once

#include "Audio.h"

namespace Glengine
{
	class ServiceLocator final
	{
	public:
		static void Init( )
		{
			m_pAudioSystem = &m_DefaultAudioSystem;
		}
	
		static AudioSystem& GetAudioSystem( )
		{
			return *m_pAudioSystem;
		}

		static void RegisterAudioSystem( AudioSystem* pAudioSystem )
		{
			m_pAudioSystem = (pAudioSystem != nullptr) ? pAudioSystem : &m_DefaultAudioSystem;
		}

	private:
		static AudioSystem* m_pAudioSystem;
		static NullAudioSystem m_DefaultAudioSystem;
	};
}