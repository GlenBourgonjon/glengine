#include "MiniginPCH.h"
#include "HealthManager.h"
#include "GameObject.h"

namespace Glengine
{
	HealthComponent* HealthManager::AddComponent( GameObject* pObject )
	{
		if (m_Components.count(pObject) == 0)
		{
			m_Components.emplace(std::make_pair(pObject, HealthComponent{ pObject }));
			HealthComponent* pComponent = &m_Components.at(pObject);
			return pComponent;
		}
		else
			return nullptr;
	}

	HealthComponent* HealthManager::GetComponent( GameObject* pObject )
	{
		if (m_Components.count(pObject) > 0)
			return &m_Components.at(pObject);
		else
			return nullptr;
	}

	bool HealthManager::RemoveComponent( GameObject* pObject )
	{
		return (m_Components.erase(pObject) > 0);
	}

	void HealthManager::RemoveAllComponents( )
	{
		m_Components.clear();
	}

	void HealthManager::Update( )
	{
		for (std::pair<GameObject* const, HealthComponent>& pair : m_Components)
		{
			HealthComponent& component = pair.second;
			if (component.m_NewHealth != component.m_Health)
			{
				component.NotifyObservers(component.m_NewHealth - component.m_Health);
				component.m_Health = component.m_NewHealth;
				if (!component.m_IsDead && component.m_Health <= 0)
					component.m_IsDead = true;
			}
		}
	}
}