#pragma once

#include <unordered_map>
#include <functional>

#include "Singleton.h"
#include "Component.h"

namespace Glengine
{
	class ScoreComponent final : public Component
	{
	public:
		int GetScore( ) const
		{
			return m_NewScore;
		}

		void AddScore( const int score )
		{
			m_NewScore = m_Score + score;
		}

	private:
		explicit ScoreComponent( GameObject* pObject )
			: Component{ pObject }
			, m_Score{ 0 }
			, m_NewScore{ 0 }
		{}

		int m_Score;
		int m_NewScore;

		friend class ScoreManager;
	};

	class ScoreManager final : public Singleton<ScoreManager>
	{
	public:
		ScoreComponent* AddComponent( GameObject* pObject );
		ScoreComponent* GetComponent( GameObject* pObject );
		bool RemoveComponent( GameObject* pObject );
		void RemoveAllComponents( );

	private:
		ScoreManager( ) = default;

		void Update( );

		std::unordered_map<GameObject*, ScoreComponent> m_Components;

		friend class Singleton<ScoreManager>;
		friend class Scene;
	};
}