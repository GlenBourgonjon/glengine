#pragma once

#include "Singleton.h"

namespace Glengine
{
	class Texture2D;
	class Font;

	class ResourceManager final : public Singleton<ResourceManager>
	{
	public:
		void Init( const std::string& data );
		std::shared_ptr<Texture2D> LoadTexture( const std::string& file, const int columns, const int rows ) const;
		std::shared_ptr<Font> LoadFont( const std::string& file, const unsigned int size ) const;

	private:
		ResourceManager( ) = default;

		std::string m_DataPath;

		friend class Singleton<ResourceManager>;
	};
}
