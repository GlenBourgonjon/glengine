#pragma once

#include <SDL_mixer.h>

namespace Glengine
{
	class SoundEffect final
	{
	public:
		explicit SoundEffect( const std::string& path );
		~SoundEffect( );
		SoundEffect( const SoundEffect& other) = delete;
		SoundEffect& operator=( const SoundEffect& other ) = delete;
		SoundEffect( SoundEffect&& other ) = delete;
		SoundEffect& operator=( SoundEffect&& other ) = delete;

		bool IsLoaded( ) const;
		bool Play( int loops ) const;
		void SetVolume( int value );
		int GetVolume( ) const;

	private:
		Mix_Chunk* m_pMixChunk;
	};
}