#pragma once

#include <unordered_map>
#include "Singleton.h"
#include "Component.h"

namespace Glengine
{
	class BehaviorTree;

	class BehaviorTreeComponent final : public Component
	{
	public:
		void SetTree( BehaviorTree* pTree )
		{
			m_pTree = pTree;
		}

	private:
		explicit BehaviorTreeComponent( GameObject* pObject )
			: Component{ pObject }
			, m_pTree{ nullptr }
		{}

		BehaviorTree* m_pTree;

		friend class BehaviorTreeManager;
	};

	class BehaviorTreeManager final : public Singleton<BehaviorTreeManager>
	{
	public:
		BehaviorTreeComponent* AddComponent( GameObject* pObject );
		BehaviorTreeComponent* GetComponent( GameObject* pObject );
		bool RemoveComponent( GameObject* pObject );
		void RemoveAllComponents( );

	private:
		BehaviorTreeManager( ) = default;

		void Update( );

		std::unordered_map<GameObject*, BehaviorTreeComponent> m_Components;

		friend class Singleton<BehaviorTreeManager>;
		friend class Scene;
	};
}