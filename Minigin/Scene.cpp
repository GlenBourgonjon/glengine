#include "MiniginPCH.h"
#include "Scene.h"
#include "Managers.h"
#include "Input.h"
#include "GameObject.h"
#include "Time.h"

namespace Glengine
{
	int Scene::m_IdCounter = 0;

	Scene::Scene( const std::string& name )
		: m_Active{ false }
		, m_SceneID{ ++m_IdCounter }
		, m_Name{ name }
		, m_pObjects{ }
		, m_pObjectsToRemove{ }
	{}

	Scene::~Scene( )
	{
		for (GameObject* pObject : m_pObjects)
		{
			delete pObject;
		}
	}

	int Scene::GetSceneID( ) const
	{
		return m_SceneID;
	}

	const std::string& Scene::GetName( ) const
	{
		return m_Name;
	}

	bool Scene::AddGameObject( GameObject* pObject )
	{
		auto it = std::find(m_pObjects.begin(), m_pObjects.end(), pObject);
		if (it == m_pObjects.end())
		{
			m_pObjects.push_back(pObject);
			return true;
		}
		
		return false;
	}

	bool Scene::RemoveGameObject( GameObject* pObject )
	{
		auto it = std::find(m_pObjects.begin(), m_pObjects.end(), pObject);
		if (it == m_pObjects.end())
			return false;

		m_pObjectsToRemove.push_back(pObject);
		return true;
	}

	const std::vector<GameObject*>& Scene::GetGameObjects( ) const
	{
		return m_pObjects;
	}

	bool Scene::IsActive( ) const
	{
		return m_Active;
	}

	void Scene::Update( )
	{
		if (!m_Active)
			return;

		LogicManager::GetInstance().EarlyUpdate();

		TimerManager::GetInstance().Update();
		BehaviorTreeManager::GetInstance().Update();
		PhysicsManager::GetInstance().Update();
		HealthManager::GetInstance().Update();
		ScoreManager::GetInstance().Update();

		LogicManager::GetInstance().LateUpdate();

		TextureManager::GetInstance().Update();
		TextManager::GetInstance().Update();

		for (GameObject* pObject : m_pObjectsToRemove)
		{
			auto it = std::find(m_pObjects.begin(), m_pObjects.end(), pObject);
			if (it == m_pObjects.end())
				continue;

			m_pObjects.erase(it);
			TimerManager::GetInstance().RemoveComponent(pObject);
			BehaviorTreeManager::GetInstance().RemoveComponent(pObject);
			PhysicsManager::GetInstance().RemoveComponent(pObject);
			HealthManager::GetInstance().RemoveComponent(pObject);
			LogicManager::GetInstance().RemoveEarlyComponent(pObject);
			LogicManager::GetInstance().RemoveLateComponent(pObject);
			TextureManager::GetInstance().RemoveComponent(pObject);
			TextManager::GetInstance().RemoveComponent(pObject);
			ScoreManager::GetInstance().RemoveComponent(pObject);
			delete pObject;
		}
		m_pObjectsToRemove.clear();
	}

	void Scene::Render( )
	{
		TextureManager::GetInstance().Render();
		TextManager::GetInstance().Render();
	}

	void Scene::Activate( )
	{
		if (m_Active)
			return;
		m_Active = true;

		Init();
	}

	void Scene::Deactivate( )
	{
		if (!m_Active)
			return;
		m_Active = false;

		LogicManager::GetInstance().RemoveAllComponents();
		TimerManager::GetInstance().RemoveAllComponents();
		BehaviorTreeManager::GetInstance().RemoveAllComponents();
		PhysicsManager::GetInstance().RemoveAllComponents();
		HealthManager::GetInstance().RemoveAllComponents();
		TextureManager::GetInstance().RemoveAllComponents();
		TextManager::GetInstance().RemoveAllComponents();
		ScoreManager::GetInstance().RemoveAllComponents();
		InputManager::GetInstance().RemoveAllCommands();

		for (GameObject* pObject : m_pObjects)
		{
			delete pObject;
		}
		m_pObjects.clear();
	}
}