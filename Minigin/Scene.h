#pragma once

#include "SceneManager.h"
#include <string>

namespace Glengine
{
	class GameObject;

	class Scene
	{
	public:
		virtual ~Scene( );

		Scene( const Scene& other ) = delete;
		Scene( Scene&& other ) = delete;
		Scene& operator=( const Scene& other ) = delete;
		Scene& operator=( Scene&& other ) = delete;

		int GetSceneID( ) const;
		const std::string& GetName( ) const;
		bool AddGameObject( GameObject* pObject );
		bool RemoveGameObject( GameObject* pObject );

	protected:
		explicit Scene( const std::string& name );
		virtual void Init( ) = 0;

		const std::vector<GameObject*>& GetGameObjects( ) const;
		bool IsActive( ) const;
		void Activate( );
		void Deactivate( );

		bool m_Active;

	private:
		void Update( );
		void Render( );
		
		const int m_SceneID;
		const std::string m_Name;

		std::vector<GameObject*> m_pObjects;
		std::vector<GameObject*> m_pObjectsToRemove;

		static int m_IdCounter;

		friend class SceneManager;
	};
}
