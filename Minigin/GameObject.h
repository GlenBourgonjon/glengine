#pragma once

#pragma warning(push)
#pragma warning (disable:4201)
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#pragma warning(pop)

namespace Glengine
{
	class GameObject final
	{
	public:
		GameObject( );

		GameObject( const GameObject& other ) = delete;
		GameObject( GameObject&& other ) noexcept = delete;
		GameObject& operator=( const GameObject& other ) = delete;
		GameObject& operator=( GameObject&& other ) noexcept = delete;

		const std::string& GetTag( ) const;
		void SetTag( const std::string& tag );

		const glm::vec3& GetPosition( ) const;
		void SetPosition( const float x, const float y, const float z = 0.f );
		void SetPosition( const glm::vec3& position );
		void SetPosition( const glm::vec2& position );

		float GetRotation( ) const;
		void SetRotation( const float rotation );

		const glm::vec3& GetScale( ) const;
		void SetScale( const float x, const float y, const float z = 1.f );
		void SetScale( const glm::vec3& scale );
		void SetScale( const glm::vec2& scale );
		void SetScale( const float scale );

	private:
		float m_Rotation;
		glm::vec3 m_Position;
		glm::vec3 m_Scale;
		std::string m_Tag;
	};
}
