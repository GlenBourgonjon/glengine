#include "MiniginPCH.h"
#include "Texture2D.h"
#include <SDL.h>

namespace Glengine
{
	Texture2D::Texture2D( SDL_Texture* pTexture, const int columns, const int rows )
		: m_pTexture{ pTexture }
		, m_Columns{ columns }
		, m_Rows{ rows }
	{}

	Texture2D::~Texture2D( )
	{
		if (m_pTexture != nullptr)
			SDL_DestroyTexture(m_pTexture);
	}

	SDL_Texture* Texture2D::GetTexture( ) const
	{
		return m_pTexture;
	}

	int Texture2D::GetColumns( ) const
	{
		return m_Columns;
	}

	int Texture2D::GetRows( ) const
	{
		return m_Rows;
	}
}