#include "MiniginPCH.h"
#include "BehaviorTree.h"

namespace Glengine
{
	BehaviorState BehaviorSelector::Execute( GameObject* pObject ) const
	{
		BehaviorState state = BehaviorState::Failure;
		for (auto child : m_ChildrenBehaviors)
		{
			state = child->Execute(pObject);
			switch (state)
			{
			case BehaviorState::Failure:
				continue;
				break;
			case BehaviorState::Success:
				return state;
				break;
			case BehaviorState::Running:
				return state;
				break;
			default:
				continue;
				break;
			}
		}
		return state;
	}

	BehaviorState BehaviorSequence::Execute( GameObject* pObject ) const
	{
		BehaviorState state = BehaviorState::Success;
		for (auto child : m_ChildrenBehaviors)
		{
			state = child->Execute(pObject);
			switch (state)
			{
			case BehaviorState::Failure:
				return state;
				break;
			case BehaviorState::Success:
				continue;
				break;
			case BehaviorState::Running:
				return state;
				break;
			default:
				state = BehaviorState::Success;
				return state;
				break;
			}
		}
		return state;
	}

	BehaviorState BehaviorCollection::Execute( GameObject* pObject ) const
	{
		for (auto child : m_ChildrenBehaviors)
		{
			child->Execute(pObject);
		}
		return BehaviorState::Success;
	}

	BehaviorState BehaviorConditional::Execute( GameObject* pObject ) const
	{
		if (m_fpConditional == nullptr)
			return BehaviorState::Failure;

		switch (m_fpConditional(pObject))
		{
		case true:
			return BehaviorState::Success;
		case false:
			return BehaviorState::Failure;
		}
		return BehaviorState::Failure;
	}

	BehaviorState BehaviorAction::Execute( GameObject* pObject ) const
	{
		if (m_fpAction == nullptr)
			return BehaviorState::Failure;

		return m_fpAction(pObject);
	}
}