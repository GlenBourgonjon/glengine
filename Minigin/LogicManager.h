#pragma once

#include <unordered_map>
#include <functional>

#include "Singleton.h"
#include "Component.h"

namespace Glengine
{
	typedef std::function<void(GameObject* pObject)> LogicCallback;

	class LogicComponent final : public Component
	{
	public:
		void SetCallback( LogicCallback callback )
		{
			m_Callback = callback;
		}

	private:
		explicit LogicComponent( GameObject* pObject )
			: Component{ pObject }
			, m_Callback{ nullptr }
		{}

		LogicCallback m_Callback;

		friend class LogicManager;
	};

	class LogicManager final : public Singleton<LogicManager>
	{
	public:
		LogicComponent* AddEarlyComponent( GameObject* pObject );
		LogicComponent* GetEarlyComponent( GameObject* pObject );
		bool RemoveEarlyComponent( GameObject* pObject );
		
		LogicComponent* AddLateComponent( GameObject* pObject );
		LogicComponent* GetLateComponent( GameObject* pObject );
		bool RemoveLateComponent( GameObject* pObject );

		void RemoveAllComponents( );

	private:
		LogicManager( ) = default;

		void EarlyUpdate( );
		void LateUpdate( );

		std::unordered_map<GameObject*, LogicComponent> m_EarlyComponents;
		std::unordered_map<GameObject*, LogicComponent> m_LateComponents;

		friend class Singleton<LogicManager>;
		friend class Scene;
	};
}