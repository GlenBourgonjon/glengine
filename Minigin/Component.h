#pragma once

#include <functional>

namespace Glengine
{
	class GameObject;
	class Component;

	typedef std::function<void(Component* pComponent, const int info)> ObserverCallback;

	class Observer final
	{
	public:
		Observer( ObserverCallback callback )
			: m_Callback{ callback }
		{}

		virtual void Notify( Component* pComponent, const int info )
		{
			m_Callback(pComponent, info);
		}

	private:
		ObserverCallback m_Callback;
	};

	class Component
	{
	public:
		virtual ~Component( ) = default;

		void AddObserver( Observer* pObserver )
		{
			if (pObserver != nullptr)
				m_pObservers.push_back(pObserver);
		}

		void RemoveObserver( Observer* pObserver )
		{
			auto it = std::find(m_pObservers.cbegin(), m_pObservers.cend(), pObserver);
			if (it != m_pObservers.cend())
				m_pObservers.erase(it);
		}

	protected:
		explicit Component( GameObject* pObject )
			: m_pObject{ pObject }
		{}

		void NotifyObservers( const int info )
		{
			for (Observer* pObserver : m_pObservers)
			{
				pObserver->Notify(this, info);
			}
		}

		GameObject* GetGameObject( ) const
		{
			return m_pObject;
		}

	private:
		std::vector<Observer*> m_pObservers;
		GameObject* m_pObject;
	};
}