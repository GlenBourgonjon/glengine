#pragma once

struct _TTF_Font;

namespace Glengine
{
	class Font final
	{
	public:
		explicit Font( const std::string& fullPath, const unsigned int size );
		~Font( );
		Font( const Font& other ) = delete;
		Font( Font&& other ) noexcept = delete;
		Font& operator=( const Font& other ) = delete;
		Font& operator=( Font&& other ) noexcept = delete;
		
		_TTF_Font* GetFont( ) const;

	private:
		_TTF_Font* m_Font;
	};
}