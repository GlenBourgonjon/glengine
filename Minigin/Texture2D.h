#pragma once

struct SDL_Texture;

namespace Glengine
{
	class Texture2D final
	{
	public:
		explicit Texture2D( SDL_Texture* pTexture, const int columns = 1, const int rows = 1 );
		~Texture2D( );
		Texture2D( const Texture2D& other ) = delete;
		Texture2D( Texture2D&& other ) noexcept = delete;
		Texture2D& operator=( const Texture2D& other ) = delete;
		Texture2D& operator=( Texture2D&& other ) noexcept = delete;
		
		SDL_Texture* GetTexture( ) const;
		int GetColumns( ) const;
		int GetRows( ) const;

	private:
		SDL_Texture* m_pTexture;
		const int m_Columns;
		const int m_Rows;
	};
}