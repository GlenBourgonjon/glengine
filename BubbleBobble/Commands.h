#pragma once

#include "GameLevel.h"

#include <Command.h>
#include <ServiceLocator.h>
#include <Input.h>
#include <Time.h>
#include <Managers.h>
#include <GameObject.h>

namespace Glengine
{
	class PauseCommand final : public Command
	{
	public:
		PauseCommand( GameLevel* pLevel )
			: Command{ }
			, m_pLevel{ pLevel }
		{}

		void Execute( ) override
		{
			m_pLevel->Pause();
		}

	private:
		GameLevel* m_pLevel;
	};

	class ChangeButtonCommand final : public Command
	{
	public:
		ChangeButtonCommand( GameLevel* pLevel, const int change )
			: Command{ }
			, m_pLevel{ pLevel }
			, m_Change{ change }
		{}

		void Execute( ) override
		{
			m_pLevel->GetGameUI().ChangeButton(m_Change);
		}

	private:
		GameLevel* m_pLevel;
		const int m_Change;
	};

	class ShootBubbleCommand final : public Command
	{
	public:
		explicit ShootBubbleCommand( GameObject* pObject, const int playerID )
			: Command{ }
			, m_pObject{ pObject }
			, m_PlayerID{ playerID }
		{}

		void Execute( ) override
		{
			PhysicsComponent* pPlayerPhysics = PhysicsManager::GetInstance().GetComponent(m_pObject);
			if (pPlayerPhysics == nullptr || pPlayerPhysics->IsStatic() || TextureManager::GetInstance().GetComponent(m_pObject) == nullptr)
				return;

			// Finding the spawn position and creating the bubble.
			int dir = pPlayerPhysics->GetHorizontalDirection();
			if (dir == 0)
				dir = 1;
			glm::vec3 pos = m_pObject->GetPosition();
			pos.x += dir * 30.f;
			GameObject* pObject = new GameObject{ };
			pObject->SetTag("Bubble");
			pObject->SetPosition(pos);
			pObject->SetScale(2.f);

			// Adding a texture and physics component.
			TextureComponent* pTextureComponent = TextureManager::GetInstance().AddComponent(pObject);
			pTextureComponent->SetTexture(2);
			pTextureComponent->SetMinMaxSprite(180 + 15 * m_PlayerID, 180 + 15 * m_PlayerID + 15);
			pTextureComponent->SetAnimated(true);
			pTextureComponent->SetAnimationSpeed(5.f);
			pTextureComponent->SetSpriteInterval(2);
			PhysicsComponent* pPhysicsComponent = PhysicsManager::GetInstance().AddComponent(pObject);
			pPhysicsComponent->SetBoundingBox({ 5.f, 5.f, 27.f, 27.f });
			pPhysicsComponent->SetStatic(false);
			pPhysicsComponent->SetGravity({ 0.f, 1.f });
			pPhysicsComponent->SetTrigger(true);
			pPhysicsComponent->SetLayer(1 << 1);
			pPhysicsComponent->SetVelocity({dir * 150.f, 0.f});
			pPhysicsComponent->SetFriction(0.5f);
			pPhysicsComponent->SetCollisionFlags(0b00000000);

			// The bubble should destroy itself after a while.
			auto timerCallback = [](GameObject* pObject)
			{
				pObject->SetPosition(0.f, 0.f, 0.f);
				SceneManager::GetInstance().GetActiveScene()->RemoveGameObject(pObject);
			};
			TimerComponent* pTimerComponent = TimerManager::GetInstance().AddComponent(pObject);
			pTimerComponent->Start(4.f, timerCallback, false);

			SceneManager::GetInstance().GetActiveScene()->AddGameObject(pObject);
			ServiceLocator::GetAudioSystem().PlayEffect("Resources/Sounds/shoot.mp3");
		}

	private:
		GameObject* m_pObject;
		const int m_PlayerID;
	};

	class HorizontalMoveCommand final : public Command
	{
	public:
		explicit HorizontalMoveCommand( PhysicsComponent* pPhysicsComponent, const float velocity )
			: Command{ }
			, m_PhysicsComponent{ pPhysicsComponent }
			, m_Velocity{ velocity }
		{}

		void Execute( ) override
		{
			if (!m_PhysicsComponent->IsStatic())
				m_PhysicsComponent->SetVelocityX(m_Velocity);
		}

	private:
		const float m_Velocity;
		PhysicsComponent* m_PhysicsComponent;
	};

	class JumpCommand final : public Command
	{
	public:
		explicit JumpCommand( PhysicsComponent* pPhysicsComponent, const float force )
			: Command{ }
			, m_PhysicsComponent{ pPhysicsComponent }
			, m_Force{ force }
		{}

		void Execute( ) override
		{
			if (m_PhysicsComponent->IsGrounded() && !m_PhysicsComponent->IsStatic())
			{
				m_PhysicsComponent->SetVelocityY(m_Force);
				ServiceLocator::GetAudioSystem().PlayEffect("Resources/Sounds/jump.mp3");
			}
		}

	private:
		const float m_Force;
		PhysicsComponent* m_PhysicsComponent;
	};

	class BubbleJumpCommand final : public Command
	{
	public:
		explicit BubbleJumpCommand( PhysicsComponent* pPhysicsComponent, const float force, const int controllerID )
			: Command{ }
			, m_PhysicsComponent{ pPhysicsComponent }
			, m_ControllerID{ controllerID }
			, m_Force{ force }
		{}

		void Execute( ) override
		{
			if (InputManager::GetInstance().IsControllerButton(Input::ControllerButton::A, m_ControllerID))
			{
				m_PhysicsComponent->SetVelocityY(m_Force);
				ServiceLocator::GetAudioSystem().PlayEffect("Resources/Sounds/jump.mp3");
			}
		}

	private:
		const float m_Force;
		const int m_ControllerID;
		PhysicsComponent* m_PhysicsComponent;
	};

	class ThrowCommand final : public Command
	{
	public:
		explicit ThrowCommand( GameObject* pObject )
			: Command{ }
			, m_pObject{ pObject }
		{}

		void Execute( ) override
		{
			// Spawning the boulder.
			GameObject* pObject = new GameObject{ };
			pObject->SetTag("Boulder");
			pObject->SetPosition(m_pObject->GetPosition());
			pObject->SetScale(2.f);

			// Adding a texture and physics component to the boulder.
			TextureComponent* pTextureComponent = TextureManager::GetInstance().AddComponent(pObject);
			pTextureComponent->SetTexture(3);
			pTextureComponent->SetSprite(13 * 15 + 6);
			PhysicsComponent* pPhysicsComponent = PhysicsManager::GetInstance().AddComponent(pObject);
			const int dir = PhysicsManager::GetInstance().GetComponent(m_pObject)->GetHorizontalDirection();
			pPhysicsComponent->SetBoundingBox({ 5.f, 5.f, 27.f, 27.f });
			pPhysicsComponent->SetStatic(false);
			pPhysicsComponent->SetGravity({ 0.f, 5.f });
			pPhysicsComponent->SetTrigger(true);
			pPhysicsComponent->SetLayer(1 << 1);
			pPhysicsComponent->SetVelocity({dir * 250.f, -200.f});
			pPhysicsComponent->SetFriction(0.7f);
			pPhysicsComponent->SetCollisionFlags(0b00000000);

			// Adding rotation functionality and make sure it deletes itself after a while.
			LogicComponent* pLogicComponent = LogicManager::GetInstance().AddLateComponent(pObject);
			auto lambda = [dir]( GameObject* pObject )
			{
				pObject->SetRotation(pObject->GetRotation() + dir * float(Time::GetDeltaTime()) * 300);

				glm::vec3 pos = pObject->GetPosition();
				if (pos.x < -32.f || pos.x > 640.f || pos.y > 480.f)
				{
					pObject->SetPosition(pos);
					pObject->SetPosition(0.f, 0.f, 0.f);
					TextureManager::GetInstance().RemoveComponent(pObject);
					PhysicsManager::GetInstance().RemoveComponent(pObject);
				}
			};
			pLogicComponent->SetCallback(lambda);

			SceneManager::GetInstance().GetActiveScene()->AddGameObject(pObject);
			ServiceLocator::GetAudioSystem().PlayEffect("Resources/Sounds/shoot.mp3");
		}

	private:
		GameObject* m_pObject;
	};

	class SpawnItemCommand final : public Command
	{
	public:
		explicit SpawnItemCommand( const int itemID, GameLevel* pLevel )
			: Command{ }
			, m_ItemID{ itemID }
			, m_pLevel{ pLevel }
		{}

		void Execute( ) override
		{
			// Spawn the item.
			GameObject* pObject = new GameObject{ };
			pObject->SetTag("Item" + std::to_string(m_ItemID));
			pObject->SetPosition({ 128.f + (rand() % (640 - 228)), 40.f + (rand() % (480 - 144)) });
			pObject->SetScale(2.f);

			// Adding a texture and physics component to the item.
			TextureComponent* pTextureComponent = TextureManager::GetInstance().AddComponent(pObject);
			pTextureComponent->SetTexture(3);
			pTextureComponent->SetSprite(13 * 15 + 10 - 2 * m_ItemID );
			PhysicsComponent* pPhysicsComponent = PhysicsManager::GetInstance().AddComponent(pObject);
			pPhysicsComponent->SetBoundingBox({ 5.f, 5.f, 27.f, 32.f });
			pPhysicsComponent->SetStatic(false);
			pPhysicsComponent->SetGravity({ 0.f, 10.f });
			pPhysicsComponent->SetTrigger(true);
			pPhysicsComponent->SetLayer(1 << 1);
			pPhysicsComponent->SetCollisionFlags(0b00000001);

			// The item should destroy itself after a while.
			auto timerCallback = [](GameObject* pObject)
			{
				PhysicsManager::GetInstance().RemoveComponent(pObject);
				TextureManager::GetInstance().RemoveComponent(pObject);
				pObject->SetPosition(0.f, 0.f, 0.f);
			};
			TimerComponent* pTimerComponent = TimerManager::GetInstance().AddComponent(pObject);
			pTimerComponent->Start(5.f, timerCallback, false);

			m_pLevel->AddItem(pObject);
		}

	private:
		int m_ItemID;
		GameLevel* m_pLevel;
	};
}