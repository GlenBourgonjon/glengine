#include "StartScreen.h"
#include "GameLevel.h"

#include <Input.h>
#include <GameObject.h>
#include <Managers.h>
#include <ServiceLocator.h>

using namespace Glengine;

StartScreen::StartScreen( )
	: Scene{ "Start" }
	, m_SelectedButton{ 0 }
	, m_Controller1{ -1 }
	, m_Controller2{ -2 }
	, m_pButtonIndication{ nullptr }
	, m_pButtons{ }
	, m_pControllerNames{ }
{}

void StartScreen::Init( )
{
	// Adding all necessary textures.
	TextureManager::GetInstance().AddTexture("blocks.png", 10, 10);
	TextureManager::GetInstance().AddTexture("sprites0.png", 15, 16);
	TextureManager::GetInstance().AddTexture("sprites1.png", 15, 16);
	TextureManager::GetInstance().AddTexture("sprites3.png", 15, 16);
	TextureManager::GetInstance().AddTexture("startscreen.png");

	// Adding input checks for the menu.
	GameObject* pObject = new GameObject{};
	LogicComponent* pLogic = LogicManager::GetInstance().AddLateComponent(pObject);
	auto gameLogic = [this](GameObject*)
	{
		if (InputManager::GetInstance().IsButtonDown('A'))
			ChoosePlayer(1, -1);
		else if (InputManager::GetInstance().IsButtonDown('D'))
			ChoosePlayer(2, -1);

		if (InputManager::GetInstance().IsButtonDown('S'))
			ChangeButton(1);
		else if (InputManager::GetInstance().IsButtonDown('W'))
			ChangeButton(-1);

		for (int i = 0; i < 4; ++i)
		{
			if (InputManager::GetInstance().IsControllerButtonDown(ControllerButton::Left, i))
				ChoosePlayer(1, i);
			else if (InputManager::GetInstance().IsControllerButtonDown(ControllerButton::Right, i))
				ChoosePlayer(2, i);

			if (InputManager::GetInstance().IsControllerButtonDown(ControllerButton::Down, i))
				ChangeButton(1);
			else if (InputManager::GetInstance().IsControllerButtonDown(ControllerButton::Up, i))
				ChangeButton(-1);
		}

		if (InputManager::GetInstance().IsButtonDown(' ') || InputManager::GetInstance().IsControllerButtonDown(ControllerButton::A, 0) || InputManager::GetInstance().IsControllerButtonDown(ControllerButton::A, 1))
		{
			if (m_SelectedButton > 0 && (m_Controller1 == -2 || m_Controller2 == -2))
				return;
			if (m_SelectedButton == 0 && m_Controller1 == -2)
				return;
			ServiceLocator::GetAudioSystem().PlayEffect("Resources/Sounds/button.mp3");
			GameLevel* pLevel = static_cast<GameLevel*>(SceneManager::GetInstance().GetScene(1));
			pLevel->SetMode(m_SelectedButton, m_Controller1, m_Controller2);
			SceneManager::GetInstance().SetActiveScene(1);
		}
	};
	pLogic->SetCallback(gameLogic);

	// Draw the background.
	TextureComponent* pTextureComponent = TextureManager::GetInstance().AddComponent(pObject);
	pTextureComponent->SetTexture(4);
	AddGameObject(pObject);

	// Adding all UI stuff.

	m_pButtonIndication = new GameObject{};
	pTextureComponent = TextureManager::GetInstance().AddComponent(m_pButtonIndication);
	pTextureComponent->SetTexture(3);
	pTextureComponent->SetSprite(90);
	m_pButtonIndication->SetPosition(400, 276);
	m_pButtonIndication->SetScale(2);
	AddGameObject(m_pButtonIndication);

	m_SelectedButton = 0;
	m_pButtons.clear();
	int id = TextManager::GetInstance().AddFont("Lingua.otf", 28);
	pObject = new GameObject{ };
	TextComponent* pTextComponent = TextManager::GetInstance().AddComponent(pObject);
	pTextComponent->SetColor({ 255, 255, 255 });
	pTextComponent->SetFont(id);
	pTextComponent->SetText("SOLO MODE");
	pObject->SetPosition(445, 280);
	AddGameObject(pObject);
	m_pButtons.push_back(pObject);

	pObject = new GameObject{ };
	pTextComponent = TextManager::GetInstance().AddComponent(pObject);
	pTextComponent->SetColor({ 155, 155, 155 });
	pTextComponent->SetFont(id);
	pTextComponent->SetText("CO-OP MODE");
	pObject->SetPosition(445, 320);
	AddGameObject(pObject);
	m_pButtons.push_back(pObject);

	pObject = new GameObject{ };
	pTextComponent = TextManager::GetInstance().AddComponent(pObject);
	pTextComponent->SetColor({ 155, 155, 155 });
	pTextComponent->SetFont(id);
	pTextComponent->SetText("VS MODE");
	pObject->SetPosition(445, 360);
	AddGameObject(pObject);
	m_pButtons.push_back(pObject);

	m_pControllerNames.clear();
	id = TextManager::GetInstance().AddFont("Lingua.otf", 20);
	pObject = new GameObject{ };
	pTextComponent = TextManager::GetInstance().AddComponent(pObject);
	pTextComponent->SetColor({ 255, 255, 255 });
	pTextComponent->SetFont(id);
	pTextComponent->SetText(GetControllerName(m_Controller1));
	pObject->SetPosition(375, 455);
	AddGameObject(pObject);
	m_pControllerNames.push_back(pObject);

	pObject = new GameObject{ };
	pTextComponent = TextManager::GetInstance().AddComponent(pObject);
	pTextComponent->SetColor({ 255, 255, 255 });
	pTextComponent->SetFont(id);
	pTextComponent->SetText(GetControllerName(m_Controller2));
	pObject->SetPosition(520, 455);
	AddGameObject(pObject);
	m_pControllerNames.push_back(pObject);

	// Play the soundtrack.
	ServiceLocator::GetAudioSystem().PlayStream("Resources/Sounds/startscreen.mp3", true);
}

void StartScreen::ChoosePlayer( const int player, const int controller )
{
	// Logic for when players choose who they want to play as.
	if (player == 1)
	{
		if (m_Controller1 == controller)
			return;
		int oldController = m_Controller1;
		m_Controller1 = controller;
		TextComponent* pTextComponent = TextManager::GetInstance().AddComponent(m_pControllerNames[0]);
		pTextComponent->SetText(GetControllerName(m_Controller1));
		if (m_Controller1 == m_Controller2 && m_Controller1 != -2)
			ChoosePlayer(player % 2 + 1, -2);
		if (m_Controller2 == -2 && oldController != -2)
			ChoosePlayer(player % 2 + 1, oldController);
		ServiceLocator::GetAudioSystem().PlayEffect("Resources/Sounds/button.mp3");
	}
	else
	{
		if (m_Controller2 == controller)
			return;
		int oldController = m_Controller2;
		m_Controller2 = controller;
		TextComponent* pTextComponent = TextManager::GetInstance().AddComponent(m_pControllerNames[1]);
		pTextComponent->SetText(GetControllerName(m_Controller2));
		if (m_Controller1 == m_Controller2 && m_Controller1 != -2)
			ChoosePlayer(player % 2 + 1, -2);
		if (m_Controller1 == -2 && oldController != -2)
			ChoosePlayer(player % 2 + 1, oldController);
		ServiceLocator::GetAudioSystem().PlayEffect("Resources/Sounds/button.mp3");
	}
}

void StartScreen::ChangeButton( const int change )
{
	TextManager::GetInstance().GetComponent(m_pButtons[m_SelectedButton])->SetColor({ 155, 155, 155 });

	// Change the selected button.
	m_SelectedButton += change;
	if (m_SelectedButton < 0)
		m_SelectedButton += int(m_pButtons.size());
	else if (m_SelectedButton >= int(m_pButtons.size()))
		m_SelectedButton -= int(m_pButtons.size());

	TextManager::GetInstance().GetComponent(m_pButtons[m_SelectedButton])->SetColor({ 255, 255, 255 });
	m_pButtonIndication->SetPosition(400, m_pButtons[m_SelectedButton]->GetPosition().y - 4);
	ServiceLocator::GetAudioSystem().PlayEffect("Resources/Sounds/button.mp3");
}

std::string StartScreen::GetControllerName( const int controller )
{
	switch (controller)
	{
	case -1:
		return "Keyboard";
	case -2:
		return "None";
	default:
		return "Controller " + std::to_string(controller + 1);
	}
}