#pragma once

#include <Scene.h>

using namespace Glengine;

class StartScreen final : public Scene
{
public:
	StartScreen( );

	StartScreen( const StartScreen& other ) = delete;
	StartScreen( StartScreen&& other ) = delete;
	StartScreen& operator=( const StartScreen& other ) = delete;
	StartScreen& operator=( StartScreen&& other ) = delete;

	void ChoosePlayer( const int player, const int controller );
	void ChangeButton( const int change );
	std::string GetControllerName( const int controller );

protected:
	void Init( ) override;

private:
	int m_SelectedButton;
	int m_Controller1;
	int m_Controller2;
	GameObject* m_pButtonIndication;
	std::vector<GameObject*> m_pButtons;
	std::vector<GameObject*> m_pControllerNames;
};