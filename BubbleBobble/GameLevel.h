#pragma once

#include <Scene.h>
#include "GameUI.h"
#include "CharacterSpawner.h"

namespace Glengine
{
	class BehaviorTree;
	class Observer;

	class GameLevel final : public Scene
	{
	public:
		GameLevel( );
		~GameLevel( ) = default;
		
		GameLevel( const GameLevel& other ) = delete;
		GameLevel( GameLevel&& other ) = delete;
		GameLevel& operator=( const GameLevel& other ) = delete;
		GameLevel& operator=( GameLevel&& other ) = delete;

		void SetMode( const int mode, const int controller1, const int controller2 = -2 );
		void Pause( );
		const std::vector<GameObject*>& GetPlayers( ) const;
		void AddPlayer( GameObject* pPlayer );
		void AddItem( GameObject* pItem );
		GameUI& GetGameUI( );

	protected:
		void Init( ) override;

	private:
		void SpawnLevel( const int newLevel );
		void CreateWallVisual( const int column, const int row, const int textureID, const int spriteID );
		void CreateWallCollider( const int column1, const int column2, const int row );

		float m_Transition;
		int m_CurrentLevel;

		std::vector<GameObject*> m_pPreviousLevelObjects;
		std::vector<GameObject*> m_pCurrentLevelObjects;
		std::vector<GameObject*> m_pPlayers;
		std::vector<GameObject*> m_pItems;

		int m_Controller1;
		int m_Controller2;
		int m_GameMode;

		GameUI m_GameUI;
		CharacterSpawner m_CharacterSpawner;
	};
}