#include "GameLevel.h"
#include "Commands.h"

#include <BinaryReaderWriter.h>
#include <Managers.h>
#include <Input.h>
#include <GameObject.h>
#include <ServiceLocator.h>

namespace Glengine
{
	GameLevel::GameLevel( )
		: Glengine::Scene{ "Game" }
		, m_Transition{ 0.f }
		, m_CurrentLevel{ 0 }
		, m_pPreviousLevelObjects{ }
		, m_pCurrentLevelObjects{ }
		, m_pPlayers{ }
		, m_pItems{ }
		, m_Controller1{ -2 }
		, m_Controller2{ -2 }
		, m_GameMode{ 0 }
		, m_GameUI{ this }
		, m_CharacterSpawner{ this }
	{}

	void GameLevel::SetMode( const int mode, const int controller1, const int controller2 )
	{
		m_Controller1 = controller1;
		m_Controller2 = controller2;
		m_GameMode = mode;
	}

	void GameLevel::Pause( )
	{
		bool pause = Time::GetGameSpeed() > FLT_EPSILON;
		Time::SetGameSpeed(pause ? 0.f : 1.f);
		InputManager::GetInstance().SetLayoutID(abs(InputManager::GetInstance().GetLayoutID() - 1));
		m_GameUI.Pause(pause);
	}

	const std::vector<GameObject*>& GameLevel::GetPlayers( ) const
	{
		return m_pPlayers;
	}

	void GameLevel::AddPlayer( GameObject* pPlayer )
	{
		AddGameObject(pPlayer);
		m_pPlayers.push_back(pPlayer);
	}

	void GameLevel::AddItem( GameObject* pItem )
	{
		AddGameObject(pItem);
		m_pItems.push_back(pItem);
	}

	GameUI& GameLevel::GetGameUI( )
	{
		return m_GameUI;
	}

	void GameLevel::Init( )
	{
		Time::SetGameSpeed(1.f);
		m_pPreviousLevelObjects.clear();
		m_pCurrentLevelObjects.clear();
		m_pPlayers.clear();
		m_pItems.clear();

		// Adding pause controls.
		std::shared_ptr<ChangeButtonCommand> pUpCommand = std::make_shared<ChangeButtonCommand>(this, -1);
		std::shared_ptr<ChangeButtonCommand> pDownCommand = std::make_shared<ChangeButtonCommand>(this, 1);
		std::shared_ptr<ChangeButtonCommand> pChooseCommand = std::make_shared<ChangeButtonCommand>(this, 0);
		InputManager::GetInstance().SetLayoutID(1);
		InputManager::GetInstance().AddInputCommand(pUpCommand, 'W', ButtonPressType::Down);
		InputManager::GetInstance().AddInputCommand(pUpCommand, ControllerButton::Up, 0, ButtonPressType::Down);
		InputManager::GetInstance().AddInputCommand(pUpCommand, ControllerButton::Up, 1, ButtonPressType::Down);
		InputManager::GetInstance().AddInputCommand(pUpCommand, ControllerButton::Up, 2, ButtonPressType::Down);
		InputManager::GetInstance().AddInputCommand(pUpCommand, ControllerButton::Up, 3, ButtonPressType::Down);
		InputManager::GetInstance().AddInputCommand(pDownCommand, 'S', ButtonPressType::Down);
		InputManager::GetInstance().AddInputCommand(pDownCommand, ControllerButton::Down, 0, ButtonPressType::Down);
		InputManager::GetInstance().AddInputCommand(pDownCommand, ControllerButton::Down, 1, ButtonPressType::Down);
		InputManager::GetInstance().AddInputCommand(pDownCommand, ControllerButton::Down, 2, ButtonPressType::Down);
		InputManager::GetInstance().AddInputCommand(pDownCommand, ControllerButton::Down, 3, ButtonPressType::Down);
		InputManager::GetInstance().AddInputCommand(pChooseCommand, ' ', ButtonPressType::Down);
		InputManager::GetInstance().AddInputCommand(pChooseCommand, ControllerButton::A, 0, ButtonPressType::Down);
		InputManager::GetInstance().AddInputCommand(pChooseCommand, ControllerButton::A, 1, ButtonPressType::Down);
		InputManager::GetInstance().AddInputCommand(pChooseCommand, ControllerButton::A, 2, ButtonPressType::Down);
		InputManager::GetInstance().AddInputCommand(pChooseCommand, ControllerButton::A, 3, ButtonPressType::Down);
		std::shared_ptr<PauseCommand> pPauseCommand = std::make_shared<PauseCommand>(this);
		InputManager::GetInstance().SetLayoutID(0);
		InputManager::GetInstance().AddInputCommand(pPauseCommand, ' ', ButtonPressType::Down);
		InputManager::GetInstance().AddInputCommand(pPauseCommand, ControllerButton::Start, 0, ButtonPressType::Down);
		InputManager::GetInstance().AddInputCommand(pPauseCommand, ControllerButton::Start, 1, ButtonPressType::Down);
		InputManager::GetInstance().AddInputCommand(pPauseCommand, ControllerButton::Start, 2, ButtonPressType::Down);
		InputManager::GetInstance().AddInputCommand(pPauseCommand, ControllerButton::Start, 3, ButtonPressType::Down);

		// Start the UI and HUD.
		m_GameUI.Init();

		// Logic for the end of the level.
		GameObject* pObject = new GameObject{};
		LogicComponent* pLogic = LogicManager::GetInstance().AddLateComponent(pObject);
		auto gameLogic = [this](GameObject*)
		{
			if (m_CharacterSpawner.GetEnemiesLeft() == 0)
			{
				m_CharacterSpawner.SetEnemiesLeft(100);
				if (m_GameMode == 2 || m_CurrentLevel == 2)
				{
					ServiceLocator::GetAudioSystem().StopStream();
					ServiceLocator::GetAudioSystem().PlayEffect("Resources/Sounds/winner.mp3");
					m_Transition = -6.f;
				}
				else
					m_Transition = -4.f;
			}
			else if (m_CharacterSpawner.GetEnemiesLeft() == 100)
			{
				m_Transition += float(Time::GetDeltaTime());
				if (m_Transition >= 0.f)
				{
					m_CharacterSpawner.SetEnemiesLeft(1000);
					m_Transition = 0.f;
					if (m_CurrentLevel < 2 && m_GameMode != 2)
					{
						SpawnLevel(m_CurrentLevel + 1);
						for (GameObject* pPlayer : m_pPlayers)
						{
							PhysicsManager::GetInstance().GetComponent(pPlayer)->SetStatic(true);
						}

						const std::vector<GameObject*>& pObjects = GetGameObjects();
						for (GameObject* pItem : m_pItems)
						{
							if (std::find(pObjects.cbegin(), pObjects.cend(), pItem) != pObjects.cend())
							{
								if (pItem->GetTag() == "Item1" || pItem->GetTag() == "Item0")
									RemoveGameObject(pItem);
							}
						}
						m_pItems.clear();
					}
					else
						SceneManager::GetInstance().SetActiveScene(0);
				}
			}
			else if (m_CharacterSpawner.GetEnemiesLeft() == 1000)
			{
				float elapsed = 200 * float(Time::GetDeltaTime());
				m_Transition += elapsed;
				if (m_Transition < 480.f)
				{
					glm::vec3 pos{};
					for (GameObject* pObject : m_pPreviousLevelObjects)
					{
						pos = pObject->GetPosition();
						pos.y -= elapsed;
						pObject->SetPosition(pos);
					}
					for (GameObject* pObject : m_pCurrentLevelObjects)
					{
						pos = pObject->GetPosition();
						pos.y -= elapsed;
						pObject->SetPosition(pos);
					}
					
					const int row = 20;
					const float cellWidth = 16.f;
					const float startX = 64.f;
					const float startY = 40.f;
					const float goalY = startY + row * cellWidth;
					for (size_t i = 0; i < m_pPlayers.size(); ++i)
					{
						const int column = 3 + int(i) * 24;
						const float goalX = startX + column * cellWidth;
						pos = m_pPlayers[i]->GetPosition();
						if (pos.x < goalX && goalX - pos.x > elapsed)
							pos.x += elapsed;
						else if (pos.x > goalX && pos.x - goalX > elapsed)
							pos.x -= elapsed;
						if (pos.y < goalY && goalY - pos.y > elapsed)
							pos.y += elapsed;
						else if (pos.y > goalY && pos.y - goalY > elapsed)
							pos.y -= elapsed;
						m_pPlayers[i]->SetPosition(pos);
					}
				}
				else
				{
					elapsed -= (m_Transition - 480.f);

					glm::vec3 pos{};
					for (GameObject* pObject : m_pCurrentLevelObjects)
					{
						pos = pObject->GetPosition();
						pos.y -= elapsed;
						pObject->SetPosition(pos);
					}

					m_CharacterSpawner.SpawnEnemies(m_CurrentLevel);
					for (GameObject* pPlayer : m_pPlayers)
					{
						PhysicsManager::GetInstance().GetComponent(pPlayer)->SetStatic(false);
					}
					for (GameObject* pObject : m_pPreviousLevelObjects)
					{
						RemoveGameObject(pObject);
					}
					m_pPreviousLevelObjects.clear();
				}
			}
		};
		pLogic->SetCallback(gameLogic);
		AddGameObject(pObject);

		// Spawning blocks and starting some level stuff.
		SpawnLevel(0);
		if (m_GameMode != 2)
			m_CharacterSpawner.SpawnEnemies(m_CurrentLevel);

		// Adding players.
		m_CharacterSpawner.CreatePlayer(3, 20, 0, 1, m_Controller1);
		if (m_GameMode == 1)
			m_CharacterSpawner.CreatePlayer(27, 20, 1, 1, m_Controller2);
		else if (m_GameMode == 2)
			m_CharacterSpawner.CreateEnemyPlayer(27, 20, 1, m_Controller2);
	}

	void GameLevel::SpawnLevel( const int newLevel )
	{
		m_pPreviousLevelObjects = m_pCurrentLevelObjects;
		m_pCurrentLevelObjects.clear();

		m_CurrentLevel = newLevel;

		// Adding the walls.
		std::ifstream inFile;
		inFile.open("Resources/Data/leveldata.dat", std::ios::in | std::ios::binary);
		if (inFile.is_open())
		{
			int startOfRow = -1;
			for (int level = 0; level <= m_CurrentLevel; ++level)
			{
				for (int i = 0; i < 25; ++i)
				{
					for (int j = 0; j < 4; ++j)
					{
						char bits;
						BinaryReaderWriter<char>::Read(inFile, bits);

						if (level != m_CurrentLevel)
							continue;

						for (int k = 0; k < 8; ++k)
						{
							if ((bits & 0b10000000 >> k) != 0)
							{
								if (startOfRow == -1)
									startOfRow = j * 8 + k;

								CreateWallVisual(j * 8 + k, i, 0, level);
							}
							else if (startOfRow != -1)
							{
								CreateWallCollider(startOfRow, j * 8 + k - 1, i);
								startOfRow = -1;
							}
						}
					}

					if (startOfRow != -1)
					{
						CreateWallCollider(startOfRow, 31, i);
						startOfRow = -1;
					}
				}
			}
			inFile.close();
		}

		// Play the soundtrack.
		ServiceLocator::GetAudioSystem().PlayStream("Resources/Sounds/soundtrack.mp3", true);
	}

	void GameLevel::CreateWallVisual( const int column, const int row, const int textureID, const int spriteID )
	{
		const float width = 16.f;
		const float startX = 64.f;
		const float startY = m_CurrentLevel == 0 ? 40.f : 520.f;

		GameObject* pObject = new GameObject{ };
		pObject->SetTag("WallVisual");
		pObject->SetPosition(startX + column * width, startY + row * width);
		pObject->SetScale(2.f);

		TextureComponent* pTexture = TextureManager::GetInstance().AddComponent(pObject);
		pTexture->SetTexture(textureID);
		pTexture->SetSprite(spriteID);

		AddGameObject(pObject);
		m_pCurrentLevelObjects.push_back(pObject);
	}

	void GameLevel::CreateWallCollider( const int column1, const int column2, const int row )
	{
		const float width = 16.f;
		const float startX = 64.f;
		const float startY = m_CurrentLevel == 0 ? 40.f : 520.f;

		GameObject* pObject = new GameObject{ };
		pObject->SetTag("WallCollider");
		pObject->SetPosition(startX + column1 * width, startY + row * width);

		PhysicsComponent* pPhysics = PhysicsManager::GetInstance().AddComponent(pObject);
		pPhysics->SetStatic(true);
		pPhysics->SetBoundingBox({ 0.f, 0.f, (column2 - column1 + 1) * width, width });

		AddGameObject(pObject);
		m_pCurrentLevelObjects.push_back(pObject);
	}
}