#ifdef _DEBUG
	#include "vld.h"
#endif

#include "Minigin.h"
#include <SceneManager.h>
#include <SDL.h>
#include <Audio.h>
#include <ServiceLocator.h>
#include "StartScreen.h"
#include "GameLevel.h"

int main( int, char*[] )
{
	using namespace Glengine;
	
	Minigin engine{ };
	engine.Initialize("BubbleBobble - Glengine", 640, 480);
	NormalAudioSystem audio{ };
	ServiceLocator::RegisterAudioSystem(&audio);

	Scene* pScene = SceneManager::GetInstance().AddScene(new StartScreen{ });
	SceneManager::GetInstance().AddScene(new GameLevel{ });
	SceneManager::GetInstance().SetActiveScene(pScene);

	engine.Run();

    return 0;
}