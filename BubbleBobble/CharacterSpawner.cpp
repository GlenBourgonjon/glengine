#include "CharacterSpawner.h"
#include "GameLevel.h"
#include "EnemyBehavior.h"
#include "Commands.h"

#include <Managers.h>
#include <BehaviorTree.h>
#include <GameObject.h>
#include <ServiceLocator.h>
#include <Input.h>
#include <BinaryReaderWriter.h>

namespace Glengine
{
	CharacterSpawner::CharacterSpawner( GameLevel* pLevel )
		: m_Gravity{ 12.f }
		, m_EnemiesLeft{ 0 }
		, m_pLevel{ pLevel }
		, m_pEnemyTrees{ }
	{
		// Creating the behavior trees for the two enemies.
		m_pEnemyTrees.push_back( new BehaviorTree{
			new BehaviorSelector{{
				new BehaviorSequence{{
					new BehaviorConditional{ IsStatic }
					}},
				new BehaviorSequence{{
					new BehaviorConditional{ IsNotGrounded },
					new BehaviorAction{ MoveLeft }
					}},
				new BehaviorSequence{{
					new BehaviorConditional{ IsNotMoving },
					new BehaviorSelector{{
						new BehaviorSequence{{
							new BehaviorConditional{ LeftOnScreen },
							new BehaviorAction{ MoveRight }
							}},
						new BehaviorAction{ MoveLeft }
						}}
					}}
				}}
			});
		m_pEnemyTrees.push_back(new BehaviorTree{
			new BehaviorSelector{{
				new BehaviorSequence{{
					new BehaviorConditional{ IsStatic }
					}},
				new BehaviorSequence{{
					new BehaviorConditional{ IsNotGrounded },
					new BehaviorAction{ ContinueMoving }
					}},
				new BehaviorSequence{{
					new BehaviorConditional{ IsNotMoving },
					new BehaviorSelector{{
						new BehaviorSequence{{
							new BehaviorConditional{ LeftOnScreen },
							new BehaviorAction{ MoveRight }
							}},
						new BehaviorAction{ MoveLeft }
						}}
					}},
				new BehaviorSequence{{
					new BehaviorConditional{ ArePlayersAbove },
					new BehaviorAction{ Jump }
					}}
				}}
			});
	}

	CharacterSpawner::~CharacterSpawner( )
	{
		for (BehaviorTree* pTree : m_pEnemyTrees)
		{
			delete pTree;
		}
	}

	void CharacterSpawner::CreatePlayer( const int column, const int row, const int playerID, const int textureID, const int controller )
	{
		const float cellWidth = 16.f;
		const float startX = 64.f;
		const float startY = 40.f;
		const float width = 32.f;

		// Spawning the player.
		GameObject* pObject = new GameObject{ };
		pObject->SetTag("Player");
		pObject->SetPosition(startX + column * cellWidth, startY + row * cellWidth);
		pObject->SetScale(2.f);

		// Texture, score and health component.
		HealthComponent* pHealthComponent = HealthManager::GetInstance().AddComponent(pObject);
		pHealthComponent->SetHealth(4);
		pHealthComponent->AddObserver(m_pLevel->GetGameUI().GetHealthObserver());
		ScoreComponent* pScoreComponent = ScoreManager::GetInstance().AddComponent(pObject);
		pScoreComponent->AddObserver(m_pLevel->GetGameUI().GetScoreObserver());
		TextureComponent* pTextureComponent = TextureManager::GetInstance().AddComponent(pObject);
		pTextureComponent->SetTexture(textureID);
		pTextureComponent->SetMinMaxSprite(30 * playerID, 30 * playerID + 15);
		pTextureComponent->SetAnimationSpeed(3.f);
		pTextureComponent->SetSpriteInterval(2);

		// Physics component and trigger.
		PhysicsComponent* pPhysicsComponent = PhysicsManager::GetInstance().AddComponent(pObject);
		pPhysicsComponent->SetStatic(false);
		pPhysicsComponent->SetLayer(1 << 1);
		pPhysicsComponent->SetCollisionFlags(0b11111101);
		pPhysicsComponent->SetFriction(1.f);
		pPhysicsComponent->SetGravity({ 0.f, m_Gravity });
		pPhysicsComponent->SetBoundingBox({ 3.f, 0.f, width - 3.f, width });
		auto collisionCallback = [this](GameObject* pObject1, GameObject* pObject2)
		{
			if (m_pLevel->GetGameUI().GetLives() > 0 && pObject2->GetTag() == "Enemy1" || pObject2->GetTag() == "Enemy2")
			{
				ServiceLocator::GetAudioSystem().PlayEffect("Resources/Sounds/hit.mp3");
				HealthManager::GetInstance().GetComponent(pObject1)->ChangeHealth(-1);
			}
		};
		pPhysicsComponent->SetCollisionCallback(collisionCallback);
		auto triggerCallback = [this, playerID](GameObject* pObject1, GameObject* pObject2)
		{
			if (pObject2->GetTag() == "Bubble")
			{
				ServiceLocator::GetAudioSystem().PlayEffect("Resources/Sounds/shoot.mp3");
				if (pObject1->GetPosition().y + 16.f < pObject2->GetPosition().y)
					BubbleJumpCommand{ PhysicsManager::GetInstance().GetComponent(pObject1), -400.f, playerID}.Execute();
				pObject2->SetPosition(0.f, 0.f, 0.f);
				m_pLevel->RemoveGameObject(pObject2);
			}
			else if (pObject2->GetTag() == "Enemy1")
			{
				ServiceLocator::GetAudioSystem().PlayEffect("Resources/Sounds/shoot.mp3");
				SpawnItemCommand{ 0, m_pLevel }.Execute();
				pObject2->SetPosition(0.f, 0.f, 0.f);
				m_pLevel->RemoveGameObject(pObject2);
				--m_EnemiesLeft;
			}
			else if (pObject2->GetTag() == "Enemy2")
			{
				ServiceLocator::GetAudioSystem().PlayEffect("Resources/Sounds/shoot.mp3");
				SpawnItemCommand{ 1, m_pLevel }.Execute();
				pObject2->SetPosition(0.f, 0.f, 0.f);
				m_pLevel->RemoveGameObject(pObject2);
				--m_EnemiesLeft;
			}
			else if (pObject2->GetTag() == "Item0")
			{
				ServiceLocator::GetAudioSystem().PlayEffect("Resources/Sounds/item.mp3");
				ScoreManager::GetInstance().GetComponent(pObject1)->AddScore(100);
				pObject2->SetPosition(0.f, 0.f, 0.f);
				m_pLevel->RemoveGameObject(pObject2);
			}
			else if (pObject2->GetTag() == "Item1")
			{
				ServiceLocator::GetAudioSystem().PlayEffect("Resources/Sounds/item.mp3");
				pObject2->SetPosition(0.f, 0.f, 0.f);
				ScoreManager::GetInstance().GetComponent(pObject1)->AddScore(200);
				m_pLevel->RemoveGameObject(pObject2);
			}
			else if (m_pLevel->GetGameUI().GetLives() > 0 && pObject2->GetTag() == "Boulder")
			{
				ServiceLocator::GetAudioSystem().PlayEffect("Resources/Sounds/hit.mp3");
				HealthManager::GetInstance().GetComponent(pObject1)->ChangeHealth(-1);
				pObject2->SetPosition(0.f, 0.f, 0.f);
				m_pLevel->RemoveGameObject(pObject2);
			}
		};
		pPhysicsComponent->SetTriggerCallback(triggerCallback);

		// Logic component for the animation of the texture.
		LogicComponent* pLogicComponent = LogicManager::GetInstance().AddLateComponent(pObject);
		auto animateTexture = []( GameObject* pObject )
		{
			PhysicsComponent* pPhysics = PhysicsManager::GetInstance().GetComponent(pObject);
			TextureComponent* pTexture = TextureManager::GetInstance().GetComponent(pObject);
			if (pPhysics == nullptr || pTexture == nullptr)
				return;

			if (!pPhysics->IsMoving())
				pTexture->SetAnimated(false);
			else
			{
				switch (pPhysics->GetHorizontalDirection())
				{
				case 1:
					pTexture->SetAnimated(true);
					pTexture->SetFlipped(false);
					break;
				case -1:
					pTexture->SetAnimated(true);
					pTexture->SetFlipped(true);
					break;
				}
			}

			const float levelHeight = 432.f;
			glm::vec3 pos = pObject->GetPosition();
			if (pos.y < 8)
			{
				pos.y += levelHeight;
				pObject->SetPosition(pos);
			}
			else if (pos.y > levelHeight + 8.f)
			{
				pos.y -= levelHeight;
				pObject->SetPosition(pos);
			}
		};
		pLogicComponent->SetCallback(animateTexture);

		m_pLevel->AddPlayer(pObject);

		// Adding input commands.
		if (controller == -1)
		{
			std::shared_ptr<HorizontalMoveCommand> pCommand1 = std::make_shared<HorizontalMoveCommand>(pPhysicsComponent, -150.f);
			InputManager::GetInstance().AddInputCommand(pCommand1, 'A', ButtonPressType::Held);
			pCommand1 = std::make_shared<HorizontalMoveCommand>(pPhysicsComponent, 150.f);
			InputManager::GetInstance().AddInputCommand(pCommand1, 'D', ButtonPressType::Held);
			std::shared_ptr<JumpCommand> pCommand2 = std::make_shared<JumpCommand>(pPhysicsComponent, -400.f);
			InputManager::GetInstance().AddInputCommand(pCommand2, 'W', ButtonPressType::Down);
			std::shared_ptr<ShootBubbleCommand> pCommand3 = std::make_shared<ShootBubbleCommand>(pObject, playerID);
			InputManager::GetInstance().AddInputCommand(pCommand3, 'E', ButtonPressType::Down);
		}
		else
		{
			std::shared_ptr<HorizontalMoveCommand> pCommand1 = std::make_shared<HorizontalMoveCommand>(pPhysicsComponent, -150.f);
			InputManager::GetInstance().AddInputCommand(pCommand1, ControllerButton::Left, controller, ButtonPressType::Held);
			pCommand1 = std::make_shared<HorizontalMoveCommand>(pPhysicsComponent, 150.f);
			InputManager::GetInstance().AddInputCommand(pCommand1, ControllerButton::Right, controller, ButtonPressType::Held);
			std::shared_ptr<JumpCommand> pCommand2 = std::make_shared<JumpCommand>(pPhysicsComponent, -400.f);
			InputManager::GetInstance().AddInputCommand(pCommand2, ControllerButton::A, controller, ButtonPressType::Down);
			std::shared_ptr<ShootBubbleCommand> pCommand3 = std::make_shared<ShootBubbleCommand>(pObject, playerID);
			InputManager::GetInstance().AddInputCommand(pCommand3, ControllerButton::B, controller, ButtonPressType::Down);
		}
	}

	void CharacterSpawner::CreateEnemyPlayer( const int column, const int row, const int textureID, const int controller )
	{
		const float cellWidth = 16.f;
		const float startX = 64.f;
		const float startY = 40.f;
		const float collisionWidth = 32.f;

		// Spawning the enemy player.
		GameObject* pObject = new GameObject{ };
		pObject->SetTag("Enemy2");
		pObject->SetPosition(startX + unsigned int(column) * cellWidth, startY + unsigned int(row) * cellWidth);
		pObject->SetScale(2.f);

		// Texture component.
		TextureComponent* pTextureComponent = TextureManager::GetInstance().AddComponent(pObject);
		pTextureComponent->SetTexture(textureID);
		pTextureComponent->SetMinMaxSprite(30 * 3, 30 * 3 + 15);
		pTextureComponent->SetAnimationSpeed(3.f);
		pTextureComponent->SetSpriteInterval(2);

		// Physics component and trigger.
		PhysicsComponent* pPhysicsComponent = PhysicsManager::GetInstance().AddComponent(pObject);
		pPhysicsComponent->SetStatic(false);
		pPhysicsComponent->SetLayer(1 << 2);
		pPhysicsComponent->SetCollisionFlags(0b11111001);
		pPhysicsComponent->SetGravity({ 0.f, m_Gravity });
		pPhysicsComponent->SetFriction( 1.f );
		pPhysicsComponent->SetBoundingBox({ 3.f, 0.f, collisionWidth - 3.f, collisionWidth });
		auto triggerCallback = [](GameObject* pObject1, GameObject* pObject2)
		{
			if (pObject2->GetTag() == "Bubble")
			{
				ServiceLocator::GetAudioSystem().PlayEffect("Resources/Sounds/caught.mp3");
				PhysicsManager::GetInstance().GetComponent(pObject1)->SetTrigger(true);
				PhysicsManager::GetInstance().GetComponent(pObject1)->SetStatic(true);
				pObject2->SetPosition(0.f, 0.f, 0.f);
				SceneManager::GetInstance().GetActiveScene()->RemoveGameObject(pObject2);

				TextureComponent* pTexture = TextureManager::GetInstance().GetComponent(pObject1);
				pTexture->SetTexture(2);
				const int startID = 15 * 15;
				pTexture->SetMinMaxSprite(startID, startID + 15);

				TimerManager::GetInstance().RemoveComponent(pObject1);
				TimerComponent* pTimer = TimerManager::GetInstance().AddComponent(pObject1);
				auto callback = [](GameObject* pObject)
				{
					PhysicsManager::GetInstance().GetComponent(pObject)->SetTrigger(false);
					PhysicsManager::GetInstance().GetComponent(pObject)->SetStatic(false);
					TextureComponent* pTexture = TextureManager::GetInstance().GetComponent(pObject);
					pTexture->SetTexture(1);
					const int startID = 30 * 3;
					pTexture->SetMinMaxSprite(startID, startID + 15);
				};
				pTimer->Start(2.f, callback);
			}
		};
		pPhysicsComponent->SetTriggerCallback(triggerCallback);

		// Logic for the texture animation.
		LogicComponent* pLogicComponent = LogicManager::GetInstance().AddLateComponent(pObject);
		auto animateTexture = [](GameObject* pObject)
		{
			PhysicsComponent* pPhysics = PhysicsManager::GetInstance().GetComponent(pObject);
			TextureComponent* pTexture = TextureManager::GetInstance().GetComponent(pObject);
			if (pPhysics == nullptr || pTexture == nullptr)
				return;

			if (pPhysics->IsStatic())
				pTexture->SetAnimated(true);
			else if (!pPhysics->IsMoving())
				pTexture->SetAnimated(false);
			else
			{
				switch (pPhysics->GetHorizontalDirection())
				{
				case 1:
					pTexture->SetAnimated(true);
					pTexture->SetFlipped(false);
					break;
				case -1:
					pTexture->SetAnimated(true);
					pTexture->SetFlipped(true);
					break;
				}
			}

			glm::vec3 pos = pObject->GetPosition();
			if (pos.y < 8)
			{
				pos.y += 400.f;
				pObject->SetPosition(pos);
			}
			else if (pos.y > 440.f)
			{
				pos.y -= 400.f;
				pObject->SetPosition(pos);
			}
		};
		pLogicComponent->SetCallback(animateTexture);

		// Input commands.
		if (controller == -1)
		{
			std::shared_ptr<HorizontalMoveCommand> pCommand1 = std::make_shared<HorizontalMoveCommand>(pPhysicsComponent, -150.f);
			InputManager::GetInstance().AddInputCommand(pCommand1, 'A', ButtonPressType::Held);
			pCommand1 = std::make_shared<HorizontalMoveCommand>(pPhysicsComponent, 150.f);
			InputManager::GetInstance().AddInputCommand(pCommand1, 'D', ButtonPressType::Held);
			std::shared_ptr<JumpCommand> pCommand2 = std::make_shared<JumpCommand>(pPhysicsComponent, -400.f);
			InputManager::GetInstance().AddInputCommand(pCommand2, 'W', ButtonPressType::Down);
			std::shared_ptr<ThrowCommand> pCommand3 = std::make_shared<ThrowCommand>(pObject);
			InputManager::GetInstance().AddInputCommand(pCommand3, 'E', ButtonPressType::Down);
		}
		else
		{
			std::shared_ptr<HorizontalMoveCommand> pCommand1 = std::make_shared<HorizontalMoveCommand>(pPhysicsComponent, -150.f);
			InputManager::GetInstance().AddInputCommand(pCommand1, ControllerButton::Left, controller, ButtonPressType::Held);
			pCommand1 = std::make_shared<HorizontalMoveCommand>(pPhysicsComponent, 150.f);
			InputManager::GetInstance().AddInputCommand(pCommand1, ControllerButton::Right, controller, ButtonPressType::Held);
			std::shared_ptr<JumpCommand> pCommand2 = std::make_shared<JumpCommand>(pPhysicsComponent, -400.f);
			InputManager::GetInstance().AddInputCommand(pCommand2, ControllerButton::A, controller, ButtonPressType::Down);
			std::shared_ptr<ThrowCommand> pCommand3 = std::make_shared<ThrowCommand>(pObject);
			InputManager::GetInstance().AddInputCommand(pCommand3, ControllerButton::B, controller, ButtonPressType::Down);
		}

		m_pLevel->AddGameObject(pObject);
		m_EnemiesLeft = 1;
	}

	int CharacterSpawner::GetEnemiesLeft( ) const
	{
		return m_EnemiesLeft;
	}
	
	void CharacterSpawner::SetEnemiesLeft( const int amount )
	{
		m_EnemiesLeft = amount;
	}

	void CharacterSpawner::SpawnEnemies( const int currentLevel )
	{
		m_EnemiesLeft = 0;

		// Adding the enemies (read from a file).
		std::ifstream inFile;
		inFile.open("Resources/Data/enemydata.dat", std::ios::in | std::ios::binary);
		if (inFile.is_open())
		{
			int level = 0;
			while (level <= currentLevel && !inFile.eof())
			{
				unsigned char bits1;
				BinaryReaderWriter<unsigned char>::Read(inFile, bits1);
				if (bits1 == 0b00000000)
				{
					++level;
					continue;
				}

				unsigned char bits2, bits3;
				BinaryReaderWriter<unsigned char>::Read(inFile, bits2);
				BinaryReaderWriter<unsigned char>::Read(inFile, bits3);

				if (level != currentLevel)
					continue;

				unsigned char column = bits1 & 0b11111000;
				column = column >> 3;
				unsigned char row = bits2 & 0b11111000;
				row = row >> 3;

				unsigned char temp = bits3 & 0b00011111;
				temp = temp << 1;
				float delay = int(temp) * 0.017f;

				int enemyID = (bits1 & 0b00000111) == 0 ? 0 : 1;
				if (enemyID == 0 && m_EnemiesLeft == 2)
					enemyID = 1;
				CreateEnemy(column, row, enemyID, 1, m_pEnemyTrees[enemyID], delay);
			}
			inFile.close();
		}
	}

	void CharacterSpawner::CreateEnemy( const unsigned char column, const unsigned char row, const int enemyID, const int textureID, BehaviorTree* pTree, const float delay )
	{
		const float cellWidth = 16.f;
		const float startX = 64.f;
		const float startY = 40.f;

		// Spawn the enemy.
		GameObject* pObject = new GameObject{ };
		pObject->SetTag("Enemy" + std::to_string(1 + enemyID));
		pObject->SetPosition(startX + unsigned int(column) * cellWidth, startY + unsigned int(row) * cellWidth);
		pObject->SetScale(2.f);

		// Texture component.
		TextureComponent* pTextureComponent = TextureManager::GetInstance().AddComponent(pObject);
		pTextureComponent->SetTexture(textureID);
		pTextureComponent->SetMinMaxSprite(30 * (2 + enemyID), 30 * (2 + enemyID) + 15);
		pTextureComponent->SetAnimationSpeed(3.f);
		pTextureComponent->SetSpriteInterval(2);

		// Timer: the physics should have a delay.
		GameObject* pTimer = new GameObject{ };
		const float gravity = m_Gravity;
		auto spawnDelay = [pTree, gravity, pObject](GameObject*)
		{
			const float collisionWidth = 32.f;

			BehaviorTreeComponent* pBehaviorTree = BehaviorTreeManager::GetInstance().AddComponent(pObject);
			pBehaviorTree->SetTree(pTree);

			PhysicsComponent* pPhysicsComponent = PhysicsManager::GetInstance().AddComponent(pObject);
			pPhysicsComponent->SetStatic(false);
			pPhysicsComponent->SetLayer(1 << 2);
			pPhysicsComponent->SetCollisionFlags(0b11111001);
			pPhysicsComponent->SetGravity({ 0.f, gravity });
			pPhysicsComponent->SetBoundingBox({ 3.f, 0.f, collisionWidth - 3.f, collisionWidth });
			auto triggerCallback = [](GameObject* pObject1, GameObject* pObject2)
			{
				if (pObject2->GetTag() == "Bubble")
				{
					ServiceLocator::GetAudioSystem().PlayEffect("Resources/Sounds/caught.mp3");
					PhysicsManager::GetInstance().GetComponent(pObject1)->SetTrigger(true);
					PhysicsManager::GetInstance().GetComponent(pObject1)->SetStatic(true);
					pObject2->SetPosition(0.f, 0.f, 0.f);
					SceneManager::GetInstance().GetActiveScene()->RemoveGameObject(pObject2);

					TextureComponent* pTexture = TextureManager::GetInstance().GetComponent(pObject1);
					pTexture->SetTexture(2);
					const int startID = (pObject1->GetTag() == "Enemy1") ? (14 * 15) : (15 * 15);
					pTexture->SetMinMaxSprite(startID, startID + 15);

					TimerManager::GetInstance().RemoveComponent(pObject1);
					TimerComponent* pTimer = TimerManager::GetInstance().AddComponent(pObject1);
					auto callback = [](GameObject* pObject)
					{
						PhysicsManager::GetInstance().GetComponent(pObject)->SetTrigger(false);
						PhysicsManager::GetInstance().GetComponent(pObject)->SetStatic(false);
						TextureComponent* pTexture = TextureManager::GetInstance().GetComponent(pObject);
						pTexture->SetTexture(1);
						const int startID = (pObject->GetTag() == "Enemy1") ? (30 * 2) : (30 * 3);
						pTexture->SetMinMaxSprite(startID, startID + 15);
					};
					pTimer->Start(3.f, callback);
				}
			};
			pPhysicsComponent->SetTriggerCallback(triggerCallback);
		};
		TimerComponent* pTimerComponent = TimerManager::GetInstance().AddComponent(pTimer);
		pTimerComponent->Start(delay, spawnDelay);
		m_pLevel->AddGameObject(pTimer);

		// Logic for animating the texture.
		LogicComponent* pLogicComponent = LogicManager::GetInstance().AddLateComponent(pObject);
		auto animateTexture = []( GameObject* pObject )
		{
			PhysicsComponent* pPhysics = PhysicsManager::GetInstance().GetComponent(pObject);
			TextureComponent* pTexture = TextureManager::GetInstance().GetComponent(pObject);
			if (pPhysics == nullptr || pTexture == nullptr)
				return;

			if (pPhysics->IsStatic())
				pTexture->SetAnimated(true);
			else if (!pPhysics->IsMoving())
				pTexture->SetAnimated(false);
			else
			{
				switch (pPhysics->GetHorizontalDirection())
				{
				case 1:
					pTexture->SetAnimated(true);
					pTexture->SetFlipped(false);
					break;
				case -1:
					pTexture->SetAnimated(true);
					pTexture->SetFlipped(true);
					break;
				}
			}

			glm::vec3 pos = pObject->GetPosition();
			if (pos.y < 8)
			{
				pos.y += 400.f;
				pObject->SetPosition(pos);
			}
			else if (pos.y > 440.f)
			{
				pos.y -= 400.f;
				pObject->SetPosition(pos);
			}
		};
		pLogicComponent->SetCallback(animateTexture);

		// Let enemy type 1 throw boulders repeatedly.
		if (enemyID == 1)
		{
			auto timerCallback2 = [](GameObject* pObject)
			{
				PhysicsComponent* pPhysics = PhysicsManager::GetInstance().GetComponent(pObject);
				if (pPhysics != nullptr && !pPhysics->IsStatic())
					ThrowCommand{ pObject }.Execute();
			};
			TimerComponent* pTimerComponent2 = TimerManager::GetInstance().AddComponent(pObject);
			pTimerComponent2->Start(2.f, timerCallback2, true);
		}

		m_pLevel->AddGameObject(pObject);
		++m_EnemiesLeft;
	}
}