#pragma once

#include "Commands.h"
#include "BehaviorTree.h"

namespace Glengine
{
	// Conditions
	
	inline bool ArePlayersAbove( GameObject* pObject )
	{
		const GameLevel* pLevel = static_cast<GameLevel*>(SceneManager::GetInstance().GetActiveScene());
		const std::vector<GameObject*>& pPlayers = pLevel->GetPlayers();
		size_t above = 0;
		for (const GameObject* pPlayer : pPlayers)
		{
			if (pPlayer->GetPosition().y + 32.f < pObject->GetPosition().y)
				++above;
		}
		return (above == pPlayers.size());
	}

	inline bool LeftOnScreen( GameObject* pObject )
	{
		return pObject->GetPosition().x < 320.f;
	}

	inline bool IsStatic( GameObject* pObject )
	{
		return PhysicsManager::GetInstance().GetComponent(pObject)->IsStatic();
	}

	inline bool IsNotGrounded( GameObject* pObject )
	{
		return !PhysicsManager::GetInstance().GetComponent(pObject)->IsGrounded();
	}

	inline bool IsNotMoving( GameObject* pObject )
	{
		return abs(PhysicsManager::GetInstance().GetComponent(pObject)->GetVelocity().x) < FLT_EPSILON;
	}

	// Actions

	inline BehaviorState ContinueMoving( GameObject* pObject )
	{
		PhysicsComponent* pPhysics = PhysicsManager::GetInstance().GetComponent(pObject);
		const int dir = pPhysics->GetHorizontalDirection();
		HorizontalMoveCommand{ pPhysics , dir * 125.f }.Execute();
		return BehaviorState::Success;
	}

	inline BehaviorState MoveLeft( GameObject* pObject )
	{
		HorizontalMoveCommand{ PhysicsManager::GetInstance().GetComponent(pObject), -125.f }.Execute();
		return BehaviorState::Success;
	}

	inline BehaviorState MoveRight( GameObject* pObject )
	{
		HorizontalMoveCommand{ PhysicsManager::GetInstance().GetComponent(pObject), 125.f }.Execute();
		return BehaviorState::Success;
	}

	inline BehaviorState Jump( GameObject* pObject )
	{
		JumpCommand{ PhysicsManager::GetInstance().GetComponent(pObject), -400.f }.Execute();
		return BehaviorState::Success;
	}
}