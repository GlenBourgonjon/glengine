#include "GameUI.h"
#include "GameLevel.h"
#include <Input.h>
#include <ServiceLocator.h>
#include <Time.h>
#include <Managers.h>
#include <GameObject.h>

namespace Glengine
{
	GameUI::GameUI( GameLevel* pLevel )
		: m_SelectedButton{ 0 }
		, m_Score{ 0 }
		, m_Lives{ 0 }
		, m_pScoreObserver{ nullptr }
		, m_pHealthObserver{ nullptr }
		, m_pScoreText{ nullptr }
		, m_pHealthText{ nullptr }
		, m_pLevel{ pLevel }
	{}

	GameUI::~GameUI( )
	{
		delete m_pScoreObserver;
		delete m_pHealthObserver;
	}

	void GameUI::Init( )
	{
		// Adding HUD.

		m_pScoreText = new GameObject{ };
		TextComponent* pText = TextManager::GetInstance().AddComponent(m_pScoreText);
		pText->SetText("Score: 0");
		pText->SetFont(0);
		pText->SetColor({ 255, 255, 0 });
		m_pScoreText->SetPosition(10, 10, 0);
		m_pLevel->AddGameObject(m_pScoreText);
		m_pHealthText = new GameObject{ };
		pText = TextManager::GetInstance().AddComponent(m_pHealthText);
		pText->SetText("Lives: 4");
		pText->SetFont(0);
		pText->SetColor({ 255, 0, 0 });
		m_pHealthText->SetPosition(535, 10, 0);
		m_pLevel->AddGameObject(m_pHealthText);

		// Adding pause buttons.

		m_pPauseButtons.clear();
		m_SelectedButton = 0;
		GameObject* pObject = new GameObject{ };
		pText = TextManager::GetInstance().AddComponent(pObject);
		pText->SetText(" ");
		pText->SetFont(0);
		pText->SetColor({ 255, 255, 255 });
		pObject->SetPosition(260, 205, 0);
		m_pLevel->AddGameObject(pObject);
		m_pPauseButtons.push_back(pObject);
		pObject = new GameObject{ };
		pText = TextManager::GetInstance().AddComponent(pObject);
		pText->SetText(" ");
		pText->SetFont(0);
		pText->SetColor({ 155, 155, 155 });
		pObject->SetPosition(250, 232, 0);
		m_pLevel->AddGameObject(pObject);
		m_pPauseButtons.push_back(pObject);

		// Adding observers.

		m_Score = 0;
		m_Lives = 4;
		if (m_pScoreObserver == nullptr)
		{
			auto scoreCallback = [this]( Component*, const int info )
			{
				m_Score += info;
				TextManager::GetInstance().GetComponent(m_pScoreText)->SetText("Score: " + std::to_string(m_Score));
			};
			m_pScoreObserver = new Observer{ scoreCallback };
			auto healthCallback = [this](Component*, const int info)
			{
				m_Lives += info;

				TextManager::GetInstance().GetComponent(m_pHealthText)->SetText("Lives: " + std::to_string(m_Lives));

				if (m_Lives == 0)
				{
					GameObject* pTextObject = new GameObject{ };
					pTextObject->SetPosition(245.f, 220.f, 0.f);
					TextComponent* pTextComponent = TextManager::GetInstance().AddComponent(pTextObject);
					pTextComponent->SetColor({ 255, 255, 255 });
					pTextComponent->SetFont(0);
					pTextComponent->SetText("GAME OVER");
					TimerComponent* pTimerComponent = TimerManager::GetInstance().AddComponent(pTextObject);
					auto timer = [](GameObject*)
					{
						SceneManager::GetInstance().SetActiveScene(0);
					};
					pTimerComponent->Start(5.f, timer);
					m_pLevel->AddGameObject(pTextObject);
					InputManager::GetInstance().SetLayoutID(2);
					ServiceLocator::GetAudioSystem().StopStream();
					ServiceLocator::GetAudioSystem().PlayEffect("Resources/Sounds/gameover.mp3");
				}
			};
			m_pHealthObserver = new Observer{ healthCallback };
		}

		// Adding an FPS counter.

		pObject = new GameObject{ };
		pText = TextManager::GetInstance().AddComponent(pObject);
		pText->SetText("FPS: 0");
		pText->SetFont(0);
		pText->SetColor({ 255, 255, 255 });
		pObject->SetPosition(10, 450, 0);
		LogicComponent* pLogic = LogicManager::GetInstance().AddLateComponent(pObject);
		auto fps = [](GameObject* pObject)
		{
			TextComponent* pText = TextManager::GetInstance().GetComponent(pObject);
			pText->SetText("FPS: " + std::to_string(Time::GetFPS()));
		};
		pLogic->SetCallback(fps);
		m_pLevel->AddGameObject(pObject);
	}

	void GameUI::ChangeButton( const int change )
	{
		if (change == 0)
		{
			// Button was selected.
			if (m_SelectedButton == 0)
				m_pLevel->Pause();
			else
				SceneManager::GetInstance().SetActiveScene(0);
		}
		else
		{
			// Selection was moved.
			m_SelectedButton = abs(m_SelectedButton - 1);
			TextComponent* pText = TextManager::GetInstance().AddComponent(m_pPauseButtons[0]);
			pText->SetText("CONTINUE");
			pText->SetColor((m_SelectedButton == 1) ? SDL_Color{ 155, 155, 155 } : SDL_Color{ 255, 255, 255 });
			pText = TextManager::GetInstance().AddComponent(m_pPauseButtons[1]);
			pText->SetText("MAIN MENU");
			pText->SetColor((m_SelectedButton == 0) ? SDL_Color{ 155, 155, 155 } : SDL_Color{ 255, 255, 255 });
		}
	}

	void GameUI::Pause( const bool pause )
	{
		TextComponent* pText = TextManager::GetInstance().GetComponent(m_pPauseButtons[0]);
		pText->SetText(pause ? "CONTINUE" : " ");
		pText = TextManager::GetInstance().GetComponent(m_pPauseButtons[1]);
		pText->SetText(pause ? "MAIN MENU" : " ");
	}

	int GameUI::GetLives( ) const
	{
		return m_Lives;
	}

	Observer* GameUI::GetScoreObserver( )
	{
		return m_pScoreObserver;
	}
	
	Observer* GameUI::GetHealthObserver( )
	{
		return m_pHealthObserver;
	}
}