#pragma once

#include <vector>

namespace Glengine
{
	class GameLevel;
	class BehaviorTree;

	class CharacterSpawner final
	{
	public:
		explicit CharacterSpawner( GameLevel* pLevel );
		~CharacterSpawner( );
		
		CharacterSpawner( const CharacterSpawner& other ) = delete;
		CharacterSpawner( CharacterSpawner&& other ) = delete;
		CharacterSpawner& operator=( const CharacterSpawner& other ) = delete;
		CharacterSpawner& operator=( CharacterSpawner&& other ) = delete;

		void SpawnEnemies( const int currentLevel );
		void CreatePlayer( const int column, const int row, const int playerID, const int textureID, const int controller );
		void CreateEnemyPlayer( const int column, const int row, const int textureID, const int controller );

		int GetEnemiesLeft( ) const;
		void SetEnemiesLeft( const int amount );

	private:
		void CreateEnemy( const unsigned char column, const unsigned char row, const int enemyID, const int textureID, BehaviorTree* pTree, const float delay );

		const float m_Gravity;
		int m_EnemiesLeft;
		GameLevel* m_pLevel;
		std::vector<BehaviorTree*> m_pEnemyTrees;
	};
}