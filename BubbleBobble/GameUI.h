#pragma once

#include <vector>

namespace Glengine
{
	class Observer;
	class GameLevel;
	class GameObject;

	class GameUI final
	{
	public:
		explicit GameUI( GameLevel* pLevel );
		~GameUI( );
		
		GameUI( const GameUI& other ) = delete;
		GameUI( GameUI&& other ) = delete;
		GameUI& operator=( const GameUI& other ) = delete;
		GameUI& operator=( GameUI&& other ) = delete;

		void Init( );
		void ChangeButton( const int change );
		void Pause( const bool pause );
		int GetLives( ) const;

		Observer* GetScoreObserver( );
		Observer* GetHealthObserver( );

	private:
		int m_SelectedButton;
		std::vector<GameObject*> m_pPauseButtons;

		int m_Score;
		int m_Lives;
		Observer* m_pScoreObserver;
		Observer* m_pHealthObserver;
		GameObject* m_pScoreText;
		GameObject* m_pHealthText;
		GameLevel* m_pLevel;
	};
}