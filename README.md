# Glengine

--- Public repository at https://gitlab.com/GlenBourgonjon/glengine ---

My very own game engine & my remake of BubbleBobble.

About the engine:
- With this engine, I wanted to go the data oriented route. There are different types of components, and they consist of data only. All logic is found within the managers of those components. So instead of updating, say, every game object with its components one after the other, and the logic being included in the component class, all components of the same type are grouped together in memory and batched per type.
- A manager goes through every component of its type, and updates the data inside those components. GameObjects are merely pointers that get used to tie together different components, but GameObjects themselves don't have any knowledge of the components they "have". One thing that the GameObjects do have, is transform information. That way, every object has a position and each component can easily access it, because the components do know what object they are connected to.
- When making a game in an engine, you want to add custom functionality of course. That's where the logic components come in. These components can contain functions that the game developer can create, and easily add to the Update loop.

Some specific comments:
- Components are also subjects, and observers can easily get added to them so they get notified of changes.
- Want to include AI in your game? The BehaviorTree class will make it easy to add functionality to your NPCs. The behavior tree logic is also multithreaded, so having a lot of NPCs in your game shouldn't be a problem.
- Physics are also in the engine (custom written).
- There's a Command class, and this can be used to add functionality whenever you need it. It is most powerful when used in the input manager. Just add a command to that manager, and specify which buttons should be assigned to it. Multiple input layouts for easy switching / disabling of commands is also supported.
- The audio system is a service locator, which makes it easy to switch between configurations.
- Playing sounds, displaying textures and rendering text is of course possible. Textures can also easily be made into sprite sheets. Animations within those sprite sheets are also supported.